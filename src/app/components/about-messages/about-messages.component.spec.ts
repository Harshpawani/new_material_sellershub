import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutMessagesComponent } from './about-messages.component';

describe('AboutMessagesComponent', () => {
  let component: AboutMessagesComponent;
  let fixture: ComponentFixture<AboutMessagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutMessagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
