import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountManageaccountComponent } from './account-manageaccount.component';

describe('AccountManageaccountComponent', () => {
  let component: AccountManageaccountComponent;
  let fixture: ComponentFixture<AccountManageaccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountManageaccountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountManageaccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
