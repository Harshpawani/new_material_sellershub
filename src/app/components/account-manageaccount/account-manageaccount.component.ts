import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-account-manageaccount',
  templateUrl: './account-manageaccount.component.html',
  styleUrls: ['./account-manageaccount.component.css']
})
export class AccountManageaccountComponent implements OnInit {

  emailUser: string;
  userPhone: string;
  companyName: string;
  city: string;
  countryName: string;
  vat: string;
  addressLine1: string;
  stateid: string;
  phoneNumber: string;
  vatnumber: string;
  addressLine2: string;
  zipCodeid: string;
  accountdata;
  resData;

  newEmail: string;
  userLoginPass: any;

  @ViewChild('closemodal') closebutton;

  constructor() { 
    this.loadAccountSetting();
  }

  ngOnInit(): void {
    
  }

  saveAccountSetting() {
    
    let JSONAccount = {
        "companyName": this.companyName, 
        "city": this.city,
        "countryName": this.countryName, 
        "vat": this.vat, 
        "addressline1": this.addressLine1, 
        "addressline2": this.addressLine2, 
        "stateId": this.stateid, 
        "phoneNumber": this.phoneNumber, 
        "vatNumber": this.vatnumber, 
        "zipcode": this.zipCodeid, 
        "emailUser": this.emailUser
    }

    Swal.fire({
      title: 'Change Account Setting',
      text: "Are you sure to change account setting ?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if(result.isConfirmed) {

        localStorage.setItem("AllAccountData", JSON.stringify(JSONAccount));
        Swal.fire({
          title: 'Success',
          text: "Account setting has successful changed",
          icon: 'success',
          timer: 2000
        });

        this.loadAccountSetting();
      }
      else{
        //
      }
    })

  }


  loadAccountSetting() {
    this.emailUser  = localStorage.getItem("userLoginEmail");
    this.phoneNumber  = localStorage.getItem("userloginPhone");
    this.accountdata = JSON.parse(localStorage.getItem("AllAccountData"));
    this.resData = this.accountdata;
    console.log(this.accountdata);
    this.companyName  = this.resData.companyName;
    this.city         = this.resData.city;
    this.addressLine1 = this.resData.addressline1;
    this.addressLine2 = this.resData.addressline2;
    this.stateid      = this.resData.stateId;
    this.vatnumber    = this.resData.vatNumber;
    this.zipCodeid    = this.resData.zipcode;
    
  }

  changeEmail() {
    Swal.fire({
      title: 'Change E-mail Address',
      text: "Are you sure to change this email address ?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if(result.isConfirmed) {
        var passStorage = localStorage.getItem('userLoginPass');
        if(this.userLoginPass == passStorage){
          Swal.fire({
            title: 'Changed',
            text: 'Your email address has changed',
            icon: 'success',
            timer: 1200
          });
          this.closebutton.nativeElement.click();
          localStorage.setItem('userLoginEmail', this.newEmail);
          this.loadAccountSetting();
        }
        else{
          Swal.fire({title: 'Error', text: 'Your admin password is incorrect', icon: 'error'});
        }
        
      }
      else{
        //
      }
    })
  }

}
