import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountManageuserComponent } from './account-manageuser.component';

describe('AccountManageuserComponent', () => {
  let component: AccountManageuserComponent;
  let fixture: ComponentFixture<AccountManageuserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountManageuserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountManageuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
