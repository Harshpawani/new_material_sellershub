import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-account-manageuser',
  templateUrl: './account-manageuser.component.html',
  styleUrls: ['./account-manageuser.component.css']
})
export class AccountManageuserComponent implements OnInit {
  emailAddress: string;
  userName: string;
  userPasswd: string;
  reuserPasswd: '';
  dataUsers;
  page=1;
  pageSize=10;
  pageSizeSelect=10;

  emailAddressEdit: string;
  userNameEdit: string;
  userPasswdEdit: string;
  reuserPasswdEdit: '';
  mailEditpermission : string;
  unameEditpermisson: string; 

  @ViewChild('closemodal') closebutton;
  @ViewChild('editModal') editModal;
  @ViewChild('permissionModal') permissionModal;
  constructor() { }

  ngOnInit(): void {
    this.getUsers();
  }
  saveNewUser() {
    var isExistData = JSON.parse(localStorage.getItem("AllUsersData"));
    if(isExistData == null) isExistData = [];

    let JSONUsers = {
        "userName": this.userName, 
        "emailAddress": this.emailAddress,
        "userPasswd": this.userPasswd
    }

    localStorage.setItem("UsersData", JSON.stringify(JSONUsers));
    isExistData.push(JSONUsers);
    localStorage.setItem("AllUsersData", JSON.stringify(isExistData));

    Swal.fire({
      title: 'Success',
      text: 'New user successfull adding',
      icon: 'success'
    });

    this.closebutton.nativeElement.click();
    this.getUsers();
  }

  getUsers() {
    this.dataUsers = JSON.parse(localStorage.getItem("AllUsersData"));
    console.log(this.dataUsers);
  }

  pageSizeChange(event){
    this.pageSizeSelect = event;
  }

  modalEdit(param, params) {
    this.editModal.nativeElement.click();
    console.log(param + ' ' + params);
    this.emailAddressEdit = params;
    this.userNameEdit     = param;
  }

  modalPermission(param, uname){
    this.permissionModal.nativeElement.click();
    console.log(param);
    this.mailEditpermission = param;
    this.unameEditpermisson = uname;
  }


  saveUserPermissions() {
    Swal.fire({
      title: 'Success',
      text: 'Permissions for user (' + this.unameEditpermisson + ') successfull saved',
      icon: 'success'
    });
    this.closebutton.nativeElement.click();
  }

  removeUser(emailAddress, userName){
    Swal.fire({
      title: 'Remove User',
      text: "Are you sure to remove user " + userName + " ?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if(result.isConfirmed) {
        var dataUser = JSON.parse(localStorage.getItem("AllUsersData"));
        var index = dataUser.findIndex(item => item.userName === userName);
        console.log('Index: ' + index);
        dataUser.splice(index, 1);
        localStorage.setItem("AllUsersData", JSON.stringify(dataUser));
        Swal.fire({
          title: 'Remove User',
          text: 'User ' + userName + ' has been removed',
          icon: 'success',
          timer: 1200
        });
        this.getUsers();
      }
      else{
        //
      }
    })
  }
}
