import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountSubscriptionsplanComponent } from './account-subscriptionsplan.component';

describe('AccountSubscriptionsplanComponent', () => {
  let component: AccountSubscriptionsplanComponent;
  let fixture: ComponentFixture<AccountSubscriptionsplanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountSubscriptionsplanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountSubscriptionsplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
