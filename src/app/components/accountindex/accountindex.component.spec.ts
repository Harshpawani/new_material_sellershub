import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountindexComponent } from './accountindex.component';

describe('AccountindexComponent', () => {
  let component: AccountindexComponent;
  let fixture: ComponentFixture<AccountindexComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccountindexComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountindexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
