import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelproductsComponent } from './channelproducts.component';

describe('ChannelproductsComponent', () => {
  let component: ChannelproductsComponent;
  let fixture: ComponentFixture<ChannelproductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelproductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelproductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
