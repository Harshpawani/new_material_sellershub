import { Component, OnInit } from '@angular/core';
import { NgbDateStruct, NgbModule} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-channelproducts',
  templateUrl: './channelproducts.component.html',
  styleUrls: ['./channelproducts.component.css']
})
export class ChannelproductsComponent implements OnInit {

  model: NgbDateStruct;
  showMe:boolean=true;
  constructor( ) { }

  ngOnInit(): void {
  }
  toggleTag(){
    this.showMe =! this.showMe
  }

  

}
