import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BackmarketIntegrationComponent } from './backmarket-integration.component';

describe('BackmarketIntegrationComponent', () => {
  let component: BackmarketIntegrationComponent;
  let fixture: ComponentFixture<BackmarketIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BackmarketIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BackmarketIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
