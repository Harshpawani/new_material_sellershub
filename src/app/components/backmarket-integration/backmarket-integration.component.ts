import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-backmarket-integration',
  templateUrl: './backmarket-integration.component.html',
  styleUrls: ['./backmarket-integration.component.css']
})
export class BackmarketIntegrationComponent implements OnInit {

  title: string;
  logo: string;
  constructor(private router: Router) { 
    this.logo = this.router.getCurrentNavigation().extras.state.logo;
    this.title = this.router.getCurrentNavigation().extras.state.title;
  }

  ngOnInit(): void {
  }

}
