import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BigEcommerceComponent } from './big-ecommerce.component';

describe('BigEcommerceComponent', () => {
  let component: BigEcommerceComponent;
  let fixture: ComponentFixture<BigEcommerceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BigEcommerceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BigEcommerceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
