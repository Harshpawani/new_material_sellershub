import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BluedartComponent } from './bluedart.component';

describe('BluedartComponent', () => {
  let component: BluedartComponent;
  let fixture: ComponentFixture<BluedartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BluedartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BluedartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
