import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BolcomIntegrationComponent } from './bolcom-integration.component';

describe('BolcomIntegrationComponent', () => {
  let component: BolcomIntegrationComponent;
  let fixture: ComponentFixture<BolcomIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BolcomIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BolcomIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
