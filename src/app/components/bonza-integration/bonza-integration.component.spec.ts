import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BonzaIntegrationComponent } from './bonza-integration.component';

describe('BonzaIntegrationComponent', () => {
  let component: BonzaIntegrationComponent;
  let fixture: ComponentFixture<BonzaIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BonzaIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BonzaIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
