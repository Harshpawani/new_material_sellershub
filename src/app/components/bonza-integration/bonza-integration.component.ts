import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bonza-integration',
  templateUrl: './bonza-integration.component.html',
  styleUrls: ['./bonza-integration.component.css']
})
export class BonzaIntegrationComponent implements OnInit {

  title: string;
  logo: string;
  constructor(private router: Router) { 
    this.logo = this.router.getCurrentNavigation().extras.state.logo;
    this.title = this.router.getCurrentNavigation().extras.state.title;
  }

  ngOnInit(): void {
  }

}
