import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CdiscountIntegrationComponent } from './cdiscount-integration.component';

describe('CdiscountIntegrationComponent', () => {
  let component: CdiscountIntegrationComponent;
  let fixture: ComponentFixture<CdiscountIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CdiscountIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CdiscountIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
