import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cdiscount-integration',
  templateUrl: './cdiscount-integration.component.html',
  styleUrls: ['./cdiscount-integration.component.css']
})
export class CdiscountIntegrationComponent implements OnInit {

  logo: string;
  listTemplate = true;
  cost_price: number;
  selling_price: number;
  constructor(private router: Router) { 
    this.logo = this.router.getCurrentNavigation().extras.state.logo;
    this.cost_price = 2000;
    this.selling_price = 2200;
  }

  ngOnInit(): void {
  }

}
