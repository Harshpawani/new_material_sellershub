import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CdiscountListingTemplatesComponent } from './cdiscount-listing-templates.component';

describe('CdiscountListingTemplatesComponent', () => {
  let component: CdiscountListingTemplatesComponent;
  let fixture: ComponentFixture<CdiscountListingTemplatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CdiscountListingTemplatesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CdiscountListingTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
