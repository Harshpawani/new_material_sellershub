import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cdiscount-listing-templates',
  templateUrl: './cdiscount-listing-templates.component.html',
  styleUrls: ['./cdiscount-listing-templates.component.css']
})
export class CdiscountListingTemplatesComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  switchtabOne() {
    document.getElementById("theone").className = "nav-link active";
    document.getElementById("thetwo").className = "nav-link";
    document.getElementById("one").className = "tab-pane active";
    document.getElementById("two").className = "tab-pane fade";
  }

  switchtabTwo() {
    document.getElementById("theone").className = "nav-link";
    document.getElementById("thetwo").className = "nav-link active";
    document.getElementById("one").className = "tab-pane fade";
    document.getElementById("two").className = "tab-pane active";
  }


  saveProduct() {
    Swal.fire({
      title: 'Success',
      text: 'Successfull to add',
      icon: 'success'
    });
  }

}
