import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelEposnowAddComponent } from './channel-eposnow-add.component';

describe('ChannelEposnowAddComponent', () => {
  let component: ChannelEposnowAddComponent;
  let fixture: ComponentFixture<ChannelEposnowAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelEposnowAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelEposnowAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
