import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelInfusionsoftAddComponent } from './channel-infusionsoft-add.component';

describe('ChannelInfusionsoftAddComponent', () => {
  let component: ChannelInfusionsoftAddComponent;
  let fixture: ComponentFixture<ChannelInfusionsoftAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelInfusionsoftAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelInfusionsoftAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
