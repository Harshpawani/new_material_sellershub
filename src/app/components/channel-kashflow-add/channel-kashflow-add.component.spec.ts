import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelKashflowAddComponent } from './channel-kashflow-add.component';

describe('ChannelKashflowAddComponent', () => {
  let component: ChannelKashflowAddComponent;
  let fixture: ComponentFixture<ChannelKashflowAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelKashflowAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelKashflowAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
