import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelMailchimpAddComponent } from './channel-mailchimp-add.component';

describe('ChannelMailchimpAddComponent', () => {
  let component: ChannelMailchimpAddComponent;
  let fixture: ComponentFixture<ChannelMailchimpAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelMailchimpAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelMailchimpAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
