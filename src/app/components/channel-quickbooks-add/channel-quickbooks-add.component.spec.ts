import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelQuickbooksAddComponent } from './channel-quickbooks-add.component';

describe('ChannelQuickbooksAddComponent', () => {
  let component: ChannelQuickbooksAddComponent;
  let fixture: ComponentFixture<ChannelQuickbooksAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelQuickbooksAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelQuickbooksAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
