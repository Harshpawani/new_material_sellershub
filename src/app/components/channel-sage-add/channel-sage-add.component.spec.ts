import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelSageAddComponent } from './channel-sage-add.component';

describe('ChannelSageAddComponent', () => {
  let component: ChannelSageAddComponent;
  let fixture: ComponentFixture<ChannelSageAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelSageAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelSageAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
