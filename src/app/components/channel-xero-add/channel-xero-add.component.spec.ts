import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelXeroAddComponent } from './channel-xero-add.component';

describe('ChannelXeroAddComponent', () => {
  let component: ChannelXeroAddComponent;
  let fixture: ComponentFixture<ChannelXeroAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelXeroAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelXeroAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
