import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelconnectorComponent } from './channelconnector.component';

describe('ChannelconnectorComponent', () => {
  let component: ChannelconnectorComponent;
  let fixture: ComponentFixture<ChannelconnectorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChannelconnectorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelconnectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
