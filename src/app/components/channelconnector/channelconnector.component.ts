import { Component, OnInit } from '@angular/core';
import { UrlAPIService } from 'src/app/services/url-api.service';
import { ChannelIntegration } from 'src/app/services/channel-integration';
import { ChannelRegistration } from 'src/app/services/channel-registration';
import { ChannelEcommerce } from 'src/app/services/channel-ecommerce';
import { Router } from '@angular/router';

@Component({
  selector: 'app-channelconnector',
  templateUrl: './channelconnector.component.html',
  styleUrls: ['./channelconnector.component.css']
})
export class ChannelconnectorComponent implements OnInit {

  url_api = "channel-integration-marketplaces.json"; //Data Source From JSON File
  url_api_channel_registration = "channel-registration.json";
  url_api_channel_ecommerce = "channel-integration-ecommerce.json";

  channelLists: ChannelIntegration[];
  channelRegistration: ChannelRegistration[];
  channelEcommerce: ChannelEcommerce[];


  constructor(private urlApi: UrlAPIService, private router: Router) { }

  ngOnInit(): void {
    this.getData();
    this.getRegistrationChannel();
    this.getEcommerceChannel();
  }


  getData() {
    this.urlApi.getChannelIntegrations(this.url_api)
        .subscribe((response) => {
          console.log(response);
          this.channelLists = response;
        })
  }
  getEcommerceChannel() {
    this.urlApi.getChannelIntegrations(this.url_api_channel_ecommerce)
        .subscribe((response) => {
          console.log(response);
          this.channelEcommerce = response;
        })
  }

  getRegistrationChannel() {
    this.urlApi.getChannelIntegrations(this.url_api_channel_registration)
        .subscribe((response) => {
          console.log(response);
          this.channelRegistration = response;
        })
  }

  gotoChannel(url, logo, title){
    console.log(url + ' ' + logo + ' ' + title);
    this.router.navigate([url], {
      state: { logo, title }
    })
  }
} 
