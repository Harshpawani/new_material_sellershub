import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClickdroplabelsetupComponent } from './clickdroplabelsetup.component';

describe('ClickdroplabelsetupComponent', () => {
  let component: ClickdroplabelsetupComponent;
  let fixture: ComponentFixture<ClickdroplabelsetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClickdroplabelsetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClickdroplabelsetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
