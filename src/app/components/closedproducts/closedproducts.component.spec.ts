import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClosedproductsComponent } from './closedproducts.component';

describe('ClosedproductsComponent', () => {
  let component: ClosedproductsComponent;
  let fixture: ComponentFixture<ClosedproductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClosedproductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClosedproductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
