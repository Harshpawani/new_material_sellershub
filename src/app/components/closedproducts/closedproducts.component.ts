import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-closedproducts',
  templateUrl: './closedproducts.component.html',
  styleUrls: ['./closedproducts.component.css']
})
export class ClosedproductsComponent implements OnInit {

  showMe:boolean=false;
  constructor( ) { }

  ngOnInit(): void {
  }
  toggleTag(){
    this.showMe =! this.showMe
  }

}
