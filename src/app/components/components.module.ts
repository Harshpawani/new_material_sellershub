import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';

import { ButtonsComponent } from './buttons/buttons.component';
import { ComponentsRoutes } from './components.routing';
import { GridSystemComponent } from './grid/grid.component';
import { IconsComponent } from './icons/icons.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { PanelsComponent } from './panels/panels.component';
import { SweetAlertComponent } from './sweetalert/sweetalert.component';
import { TypographyComponent } from './typography/typography.component';

import { MapsComponent } from "./maps/maps.component";
import { UserProfileComponent } from "./user-profile/user-profile.component";
import { TablesComponent } from "./tables/tables.component";
import { ProductsComponent } from "./products/products.component";
import { ChannelproductsComponent } from "./channelproducts/channelproducts.component";
import { ChannelconnectorComponent } from "./channelconnector/channelconnector.component";
import { StockviewComponent } from "./stockview/stockview.component";
import { ListingerrorComponent } from "./listingerror/listingerror.component";
import { ListedproductComponent } from "./listedproduct/listedproduct.component";
import { ClosedproductsComponent } from "./closedproducts/closedproducts.component";
import { SchedulelistingComponent } from "./schedulelisting/schedulelisting.component";
import { OrdersummaryComponent } from "./ordersummary/ordersummary.component";
import { McfordersComponent } from "./mcforders/mcforders.component";
import { ShippedordersComponent } from "./shippedorders/shippedorders.component";
import { ReturnedordersComponent } from "./returnedorders/returnedorders.component";
import { CancelledordersComponent } from "./cancelledorders/cancelledorders.component";
import { CustomersComponent } from "./customers/customers.component";
import { SuppliersComponent } from "./suppliers/suppliers.component";
import { NewsuppliersComponent } from "./newsuppliers/newsuppliers.component";
import { ReportSalesbyproductComponent } from "./report-salesbyproduct/report-salesbyproduct.component";
import { ReportLowstockComponent } from "./report-lowstock/report-lowstock.component";
import { ReportStockvalueComponent } from "./report-stockvalue/report-stockvalue.component";
import { ReportOrderhistoryComponent } from "./report-orderhistory/report-orderhistory.component";
import { WarehouseTransferComponent } from "./warehouse-transfer/warehouse-transfer.component";
import { WarehouseStocksummaryComponent } from "./warehouse-stocksummary/warehouse-stocksummary.component";
import { WarehousesComponent } from "./warehouses/warehouses.component";
import { WarehouseAddComponent } from "./warehouse-add/warehouse-add.component";
import { AccountManageaccountComponent } from "./account-manageaccount/account-manageaccount.component";
import { AccountManageuserComponent } from "./account-manageuser/account-manageuser.component";
import { AccountSubscriptionsComponent } from "./account-subscriptions/account-subscriptions.component";
import { AccountSubscriptionsplanComponent } from "./account-subscriptionsplan/account-subscriptionsplan.component";
import { SettingsGeneralsettingComponent } from "./settings-generalsetting/settings-generalsetting.component";
import { SettingsProductattributesComponent } from "./settings-productattributes/settings-productattributes.component";
import { SettingsBulkactionsComponent } from "./settings-bulkactions/settings-bulkactions.component";
import { SettingsBarcodemanagementComponent } from "./settings-barcodemanagement/settings-barcodemanagement.component";
import { SettingsMailsettingComponent } from "./settings-mailsetting/settings-mailsetting.component";
import { SettingsNotificationsettingComponent } from "./settings-notificationsetting/settings-notificationsetting.component";
import { SettingsInventorysycComponent } from "./settings-inventorysyc/settings-inventorysyc.component";
import { SettingsInvoiceComponent } from "./settings-invoice/settings-invoice.component";
import { AboutMessagesComponent } from "./about-messages/about-messages.component";
import { SalesOrdersComponent } from "./sales-orders/sales-orders.component";
import { FabCreateproductsComponent } from "./fab-createproducts/fab-createproducts.component";
import { FabSendtofbaComponent } from "./fab-sendtofba/fab-sendtofba.component";
import { FabProductinfbaComponent } from "./fab-productinfba/fab-productinfba.component";
import { PurchaseorderComponent } from "./purchaseorder/purchaseorder.component";
import { ManageReturnComponent } from "./manage-return/manage-return.component";
import { WaitingtolistComponent } from "./waitingtolist/waitingtolist.component";
import { ShippingcourierlistComponent } from "./shippingcourierlist/shippingcourierlist.component";
import { BluedartComponent } from "./bluedart/bluedart.component";
import { EasyparcelComponent } from "./easyparcel/easyparcel.component";
import { DpdintegrationComponent } from "./dpdintegration/dpdintegration.component";
import { RoyalmailsettingsComponent } from "./royalmailsettings/royalmailsettings.component";
import { RoyalmailslabelsetupComponent } from "./royalmailslabelsetup/royalmailslabelsetup.component";
import { RoyalmailppilistComponent } from "./royalmailppilist/royalmailppilist.component";
import { RoyalmaildmolistComponent } from "./royalmaildmolist/royalmaildmolist.component";
import { RoyalmailintereqComponent } from "./royalmailintereq/royalmailintereq.component";
import { RoyalmailchannelmapComponent } from "./royalmailchannelmap/royalmailchannelmap.component";
import { ShipstationlabelsetupComponent } from "./shipstationlabelsetup/shipstationlabelsetup.component";
import { HermessettingsComponent } from "./hermessettings/hermessettings.component";
import { MyparceldeliveryComponent } from "./myparceldelivery/myparceldelivery.component";
import { SpringglobalmailComponent } from "./springglobalmail/springglobalmail.component";
import { WndirectComponent } from "./wndirect/wndirect.component";
import { DxlabelsetupComponent } from './dxlabelsetup/dxlabelsetup.component';
import { ClickdroplabelsetupComponent } from "./clickdroplabelsetup/clickdroplabelsetup.component";
import { WebshipperlabelsetupComponent } from "./webshipperlabelsetup/webshipperlabelsetup.component";
import { UkmailsettingsComponent } from "./ukmailsettings/ukmailsettings.component";
import { UkmaillabelsetupComponent } from "./ukmaillabelsetup/ukmaillabelsetup.component";
import { YodellabelsetupComponent } from "./yodellabelsetup/yodellabelsetup.component";
import { ShippolabelsetupComponent } from "./shippolabelsetup/shippolabelsetup.component";
import { ParcelmonkeylabelsetupComponent } from "./parcelmonkeylabelsetup/parcelmonkeylabelsetup.component";
import { FedexlabelsetupComponent } from "./fedexlabelsetup/fedexlabelsetup.component";
import { InterlinklabelsetupComponent } from "./interlinklabelsetup/interlinklabelsetup.component";
import { PurchaseorderaddComponent } from "./purchaseorderadd/purchaseorderadd.component";
import { CreateProductsTofbaComponent } from "./create-products-tofba/create-products-tofba.component";
import { InvoicetemplatesComponent } from './invoicetemplates/invoicetemplates.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { ShippinglabelComponent } from './shippinglabel/shippinglabel.component';
import { PickuplistComponent } from './pickuplist/pickuplist.component';
import { PacklistComponent } from './packlist/packlist.component';
import { WhatsnewComponent } from './whatsnew/whatsnew.component';
import { AddnewproductsComponent } from "./addnewproducts/addnewproducts.component";
import { EbayIntegrationComponent } from "./ebay-integration/ebay-integration.component";
import { RakutenIntegrationComponent } from "./rakuten-integration/rakuten-integration.component";
import { SearsIntegrationComponent } from "./sears-integration/sears-integration.component";
import { CdiscountIntegrationComponent } from "./cdiscount-integration/cdiscount-integration.component";
import { CdiscountListingTemplatesComponent } from "./cdiscount-listing-templates/cdiscount-listing-templates.component";
import { PriceministerIntegrationComponent } from "./priceminister-integration/priceminister-integration.component";
import { BonzaIntegrationComponent } from "./bonza-integration/bonza-integration.component";
import { FlubitIntegrationComponent } from "./flubit-integration/flubit-integration.component";
import { EtsyIntegrationComponent } from "./etsy-integration/etsy-integration.component";
import { JetIntegrationComponent } from "./jet-integration/jet-integration.component";
import { FrugoIntegrationComponent } from "./frugo-integration/frugo-integration.component";
import { IofferIntegrationComponent } from "./ioffer-integration/ioffer-integration.component";
import { StorenvyIntegrationComponent } from "./storenvy-integration/storenvy-integration.component";
import { OpenskyIntegrationComponent } from "./opensky-integration/opensky-integration.component";
import { LazadaIntegrationComponent } from "./lazada-integration/lazada-integration.component";
import { LazadaTemplateIntegrationComponent } from "./lazada-template-integration/lazada-template-integration.component";
import { WalmartIntegrationComponent } from "./walmart-integration/walmart-integration.component";
import { BolcomIntegrationComponent } from "./bolcom-integration/bolcom-integration.component";
import { OnbuyIntegrationComponent } from "./onbuy-integration/onbuy-integration.component";
import { TescoIntegrationComponent } from "./tesco-integration/tesco-integration.component";
import { ShopcluesIntegrationComponent } from "./shopclues-integration/shopclues-integration.component";
import { FlipkartIntegrationComponent } from "./flipkart-integration/flipkart-integration.component";
import { GoogleshopingIntegrationComponent } from "./googleshoping-integration/googleshoping-integration.component";
import { ShopeeIntegrationComponent } from "./shopee-integration/shopee-integration.component";
import { LelongIntegrationComponent } from "./lelong-integration/lelong-integration.component";
import { StreetIntegrationComponent } from "./street-integration/street-integration.component";
import { SnapdealIntegrationComponent } from "./snapdeal-integration/snapdeal-integration.component";
import { BackmarketIntegrationComponent } from "./backmarket-integration/backmarket-integration.component";
import { OpenchartEcommerceComponent } from "./openchart-ecommerce/openchart-ecommerce.component";
import { MagentoEcommerceComponent } from "./magento-ecommerce/magento-ecommerce.component";
import { PrestashopEcommerceComponent } from "./prestashop-ecommerce/prestashop-ecommerce.component";
import { ShopifyEcommerceComponent } from "./shopify-ecommerce/shopify-ecommerce.component";
import { BigEcommerceComponent } from "./big-ecommerce/big-ecommerce.component";
import { WooEcommerceComponent } from "./woo-ecommerce/woo-ecommerce.component";

/**
 * From Harsh Branch
 * Changed by: Aminul Aprijal
 * Date Modified: 22/11/2021 | 11:49 Indonesian Time
 */

 import { CreateorderComponent } from "./createorder/createorder.component";
 import { FindproductlistingerrorsComponent } from "./findproductlistingerrors/findproductlistingerrors.component";
 import { ShippingrulesComponent } from "./shippingrules/shippingrules.component";
 import { AddruleComponent } from "./addrule/addrule.component";
 import { EditinvoicetemplateComponent } from "./editinvoicetemplate/editinvoicetemplate.component";
 import { EditshippinglabelComponent } from "./editshippinglabel/editshippinglabel.component";
 import { EdittelesalechannelComponent } from "./edittelesalechannel/edittelesalechannel.component";
 import { EditownstorechannelComponent } from "./editownstorechannel/editownstorechannel.component";
 import { ChannelInfusionsoftAddComponent } from "./channel-infusionsoft-add/channel-infusionsoft-add.component";
 import { ChannelMailchimpAddComponent } from "./channel-mailchimp-add/channel-mailchimp-add.component";
 import { ChannelEposnowAddComponent } from "./channel-eposnow-add/channel-eposnow-add.component";
 import { AccountindexComponent } from "./accountindex/accountindex.component";
 import { ChannelQuickbooksAddComponent } from "./channel-quickbooks-add/channel-quickbooks-add.component";
 import { ChannelSageAddComponent } from "./channel-sage-add/channel-sage-add.component";
 import { ChannelKashflowAddComponent } from "./channel-kashflow-add/channel-kashflow-add.component";
 import { ChannelXeroAddComponent } from "./channel-xero-add/channel-xero-add.component";


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ComponentsRoutes),
    FormsModule,
    MaterialModule
  ],
  declarations: [
      ButtonsComponent,
      GridSystemComponent,
      IconsComponent,
      NotificationsComponent,
      PanelsComponent,
      SweetAlertComponent,
      TypographyComponent,
      MapsComponent,
    UserProfileComponent,
    TablesComponent,
    ProductsComponent,
    ChannelproductsComponent,
    ChannelconnectorComponent,
    StockviewComponent,
    ListingerrorComponent,
    ListedproductComponent,
    ClosedproductsComponent,
    SchedulelistingComponent,
    OrdersummaryComponent,
    McfordersComponent,
    ShippedordersComponent,
    ReturnedordersComponent,
    CancelledordersComponent,
    CustomersComponent,
    SuppliersComponent,
    NewsuppliersComponent,
    ReportSalesbyproductComponent,
    ReportLowstockComponent,
    ReportStockvalueComponent,
    ReportOrderhistoryComponent,
    WarehouseTransferComponent,
    WarehouseStocksummaryComponent,
    WarehousesComponent,
    WarehouseAddComponent,
    AccountManageaccountComponent,
    AccountManageuserComponent,
    AccountSubscriptionsComponent,
    AccountSubscriptionsplanComponent,
    SettingsGeneralsettingComponent,
    SettingsProductattributesComponent,
    SettingsBulkactionsComponent,
    SettingsBarcodemanagementComponent,
    SettingsMailsettingComponent,
    SettingsNotificationsettingComponent,
    SettingsInventorysycComponent,
    SettingsInvoiceComponent,
    AboutMessagesComponent,
    SalesOrdersComponent,
    FabCreateproductsComponent,
    FabSendtofbaComponent,
    FabProductinfbaComponent,
    PurchaseorderComponent,
    ManageReturnComponent,
    WaitingtolistComponent,
    ShippingcourierlistComponent,
    BluedartComponent,
    EasyparcelComponent,
    DpdintegrationComponent,
    RoyalmailsettingsComponent,
    RoyalmailslabelsetupComponent,
    RoyalmailppilistComponent,
    RoyalmaildmolistComponent,
    RoyalmailintereqComponent,
    RoyalmailchannelmapComponent,
    ShipstationlabelsetupComponent,
    HermessettingsComponent,
    MyparceldeliveryComponent,
    SpringglobalmailComponent,
    WndirectComponent,
    DxlabelsetupComponent,
    ClickdroplabelsetupComponent,
    WebshipperlabelsetupComponent,
    UkmailsettingsComponent,
    UkmaillabelsetupComponent,
    YodellabelsetupComponent,
    ShippolabelsetupComponent,
    ParcelmonkeylabelsetupComponent,
    FedexlabelsetupComponent,
    InterlinklabelsetupComponent,
    PurchaseorderaddComponent,
    CreateProductsTofbaComponent,
    InvoicetemplatesComponent,
    InvoiceComponent,
    ShippinglabelComponent,
    PickuplistComponent,
    PacklistComponent,
    WhatsnewComponent,
    AddnewproductsComponent,
    EbayIntegrationComponent,
    RakutenIntegrationComponent,
    SearsIntegrationComponent,
    CdiscountIntegrationComponent,
    CdiscountListingTemplatesComponent,
    PriceministerIntegrationComponent,
    BonzaIntegrationComponent,
    FlubitIntegrationComponent,
    EtsyIntegrationComponent,
    JetIntegrationComponent,
    FrugoIntegrationComponent,
    IofferIntegrationComponent,
    StorenvyIntegrationComponent,
    OpenskyIntegrationComponent,
    LazadaIntegrationComponent,
    LazadaTemplateIntegrationComponent,
    WalmartIntegrationComponent,
    BolcomIntegrationComponent,
    OnbuyIntegrationComponent,
    TescoIntegrationComponent,
    ShopcluesIntegrationComponent,
    FlipkartIntegrationComponent,
    GoogleshopingIntegrationComponent,
    ShopeeIntegrationComponent,
    LelongIntegrationComponent,
    StreetIntegrationComponent,
    SnapdealIntegrationComponent,
    BackmarketIntegrationComponent,
    OpenchartEcommerceComponent,
    MagentoEcommerceComponent,
    PrestashopEcommerceComponent,
    ShopifyEcommerceComponent,
    BigEcommerceComponent,
    WooEcommerceComponent,
    CreateorderComponent,
    FindproductlistingerrorsComponent,
    ShippingrulesComponent,
    AddruleComponent,
    EditinvoicetemplateComponent,
    EditshippinglabelComponent,
    EdittelesalechannelComponent,
    EditownstorechannelComponent,
    ChannelInfusionsoftAddComponent,
    ChannelMailchimpAddComponent,
    ChannelEposnowAddComponent,
    AccountindexComponent,
    ChannelQuickbooksAddComponent,
    ChannelSageAddComponent,
    ChannelKashflowAddComponent,
    ChannelXeroAddComponent,
  ]
})

export class ComponentsModule {}
