import { Routes } from '@angular/router';

import { ButtonsComponent } from './buttons/buttons.component';
import { GridSystemComponent } from './grid/grid.component';
import { IconsComponent } from './icons/icons.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { PanelsComponent } from './panels/panels.component';
import { SweetAlertComponent } from './sweetalert/sweetalert.component';
import { TypographyComponent } from './typography/typography.component';


import { MapsComponent } from "./maps/maps.component";
import { UserProfileComponent } from "./user-profile/user-profile.component";
import { TablesComponent } from "./tables/tables.component";
import { ProductsComponent } from "./products/products.component";
import { ChannelproductsComponent } from "./channelproducts/channelproducts.component";
import { ChannelconnectorComponent } from "./channelconnector/channelconnector.component";
import { StockviewComponent } from "./stockview/stockview.component";
import { ListingerrorComponent } from "./listingerror/listingerror.component";
import { ListedproductComponent } from "./listedproduct/listedproduct.component";
import { ClosedproductsComponent } from "./closedproducts/closedproducts.component";
import { SchedulelistingComponent } from "./schedulelisting/schedulelisting.component";
import { OrdersummaryComponent } from "./ordersummary/ordersummary.component";
import { McfordersComponent } from "./mcforders/mcforders.component";
import { ShippedordersComponent } from "./shippedorders/shippedorders.component";
import { ReturnedordersComponent } from "./returnedorders/returnedorders.component";
import { CancelledordersComponent } from "./cancelledorders/cancelledorders.component";
import { CustomersComponent } from "./customers/customers.component";
import { SuppliersComponent } from "./suppliers/suppliers.component";
import { NewsuppliersComponent } from "./newsuppliers/newsuppliers.component";
import { ReportSalesbyproductComponent } from "./report-salesbyproduct/report-salesbyproduct.component";
import { ReportLowstockComponent } from "./report-lowstock/report-lowstock.component";
import { ReportStockvalueComponent } from "./report-stockvalue/report-stockvalue.component";
import { ReportOrderhistoryComponent } from "./report-orderhistory/report-orderhistory.component";
import { WarehouseTransferComponent } from "./warehouse-transfer/warehouse-transfer.component";
import { WarehouseStocksummaryComponent } from "./warehouse-stocksummary/warehouse-stocksummary.component";
import { WarehousesComponent } from "./warehouses/warehouses.component";
import { WarehouseAddComponent } from "./warehouse-add/warehouse-add.component";
import { AccountManageaccountComponent } from "./account-manageaccount/account-manageaccount.component";
import { AccountManageuserComponent } from "./account-manageuser/account-manageuser.component";
import { AccountSubscriptionsComponent } from "./account-subscriptions/account-subscriptions.component";
import { AccountSubscriptionsplanComponent } from "./account-subscriptionsplan/account-subscriptionsplan.component";
import { SettingsGeneralsettingComponent } from "./settings-generalsetting/settings-generalsetting.component";
import { SettingsProductattributesComponent } from "./settings-productattributes/settings-productattributes.component";
import { SettingsBulkactionsComponent } from "./settings-bulkactions/settings-bulkactions.component";
import { SettingsBarcodemanagementComponent } from "./settings-barcodemanagement/settings-barcodemanagement.component";
import { SettingsMailsettingComponent } from "./settings-mailsetting/settings-mailsetting.component";
import { SettingsNotificationsettingComponent } from "./settings-notificationsetting/settings-notificationsetting.component";
import { SettingsInventorysycComponent } from "./settings-inventorysyc/settings-inventorysyc.component";
import { SettingsInvoiceComponent } from "./settings-invoice/settings-invoice.component";
import { AboutMessagesComponent } from "./about-messages/about-messages.component";
import { SalesOrdersComponent } from "./sales-orders/sales-orders.component";
import { FabCreateproductsComponent } from "./fab-createproducts/fab-createproducts.component";
import { FabSendtofbaComponent } from "./fab-sendtofba/fab-sendtofba.component";
import { FabProductinfbaComponent } from "./fab-productinfba/fab-productinfba.component";
import { PurchaseorderComponent } from "./purchaseorder/purchaseorder.component";
import { ManageReturnComponent } from "./manage-return/manage-return.component";
import { WaitingtolistComponent } from "./waitingtolist/waitingtolist.component";
import { ShippingcourierlistComponent } from "./shippingcourierlist/shippingcourierlist.component";
import { BluedartComponent } from "./bluedart/bluedart.component";
import { EasyparcelComponent } from "./easyparcel/easyparcel.component";
import { DpdintegrationComponent } from "./dpdintegration/dpdintegration.component";
import { RoyalmailsettingsComponent } from "./royalmailsettings/royalmailsettings.component";
import { RoyalmailslabelsetupComponent } from "./royalmailslabelsetup/royalmailslabelsetup.component";
import { RoyalmailppilistComponent } from "./royalmailppilist/royalmailppilist.component";
import { RoyalmaildmolistComponent } from "./royalmaildmolist/royalmaildmolist.component";
import { RoyalmailintereqComponent } from "./royalmailintereq/royalmailintereq.component";
import { RoyalmailchannelmapComponent } from "./royalmailchannelmap/royalmailchannelmap.component";
import { ShipstationlabelsetupComponent } from "./shipstationlabelsetup/shipstationlabelsetup.component";
import { HermessettingsComponent } from "./hermessettings/hermessettings.component";
import { MyparceldeliveryComponent } from "./myparceldelivery/myparceldelivery.component";
import { SpringglobalmailComponent } from "./springglobalmail/springglobalmail.component";
import { WndirectComponent } from "./wndirect/wndirect.component";
import { DxlabelsetupComponent } from './dxlabelsetup/dxlabelsetup.component';
import { ClickdroplabelsetupComponent } from "./clickdroplabelsetup/clickdroplabelsetup.component";
import { WebshipperlabelsetupComponent } from "./webshipperlabelsetup/webshipperlabelsetup.component";
import { UkmailsettingsComponent } from "./ukmailsettings/ukmailsettings.component";
import { UkmaillabelsetupComponent } from "./ukmaillabelsetup/ukmaillabelsetup.component";
import { YodellabelsetupComponent } from "./yodellabelsetup/yodellabelsetup.component";
import { ShippolabelsetupComponent } from "./shippolabelsetup/shippolabelsetup.component";
import { ParcelmonkeylabelsetupComponent } from "./parcelmonkeylabelsetup/parcelmonkeylabelsetup.component";
import { FedexlabelsetupComponent } from "./fedexlabelsetup/fedexlabelsetup.component";
import { InterlinklabelsetupComponent } from "./interlinklabelsetup/interlinklabelsetup.component";
import { PurchaseorderaddComponent } from "./purchaseorderadd/purchaseorderadd.component";
import { CreateProductsTofbaComponent } from "./create-products-tofba/create-products-tofba.component";
import { InvoicetemplatesComponent } from './invoicetemplates/invoicetemplates.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { ShippinglabelComponent } from './shippinglabel/shippinglabel.component';
import { PickuplistComponent } from './pickuplist/pickuplist.component';
import { PacklistComponent } from './packlist/packlist.component';
import { WhatsnewComponent } from './whatsnew/whatsnew.component';
import { AddnewproductsComponent } from "./addnewproducts/addnewproducts.component";
import { EbayIntegrationComponent } from "./ebay-integration/ebay-integration.component";
import { RakutenIntegrationComponent } from "./rakuten-integration/rakuten-integration.component";
import { SearsIntegrationComponent } from "./sears-integration/sears-integration.component";
import { CdiscountIntegrationComponent } from "./cdiscount-integration/cdiscount-integration.component";
import { CdiscountListingTemplatesComponent } from "./cdiscount-listing-templates/cdiscount-listing-templates.component";
import { PriceministerIntegrationComponent } from "./priceminister-integration/priceminister-integration.component";
import { BonzaIntegrationComponent } from "./bonza-integration/bonza-integration.component";
import { FlubitIntegrationComponent } from "./flubit-integration/flubit-integration.component";
import { EtsyIntegrationComponent } from "./etsy-integration/etsy-integration.component";
import { JetIntegrationComponent } from "./jet-integration/jet-integration.component";
import { FrugoIntegrationComponent } from "./frugo-integration/frugo-integration.component";
import { IofferIntegrationComponent } from "./ioffer-integration/ioffer-integration.component";
import { StorenvyIntegrationComponent } from "./storenvy-integration/storenvy-integration.component";
import { OpenskyIntegrationComponent } from "./opensky-integration/opensky-integration.component";
import { LazadaIntegrationComponent } from "./lazada-integration/lazada-integration.component";
import { LazadaTemplateIntegrationComponent } from "./lazada-template-integration/lazada-template-integration.component";
import { WalmartIntegrationComponent } from "./walmart-integration/walmart-integration.component";
import { BolcomIntegrationComponent } from "./bolcom-integration/bolcom-integration.component";
import { OnbuyIntegrationComponent } from "./onbuy-integration/onbuy-integration.component";
import { TescoIntegrationComponent } from "./tesco-integration/tesco-integration.component";
import { ShopcluesIntegrationComponent } from "./shopclues-integration/shopclues-integration.component";
import { FlipkartIntegrationComponent } from "./flipkart-integration/flipkart-integration.component";
import { GoogleshopingIntegrationComponent } from "./googleshoping-integration/googleshoping-integration.component";
import { ShopeeIntegrationComponent } from "./shopee-integration/shopee-integration.component";
import { LelongIntegrationComponent } from "./lelong-integration/lelong-integration.component";
import { StreetIntegrationComponent } from "./street-integration/street-integration.component";
import { SnapdealIntegrationComponent } from "./snapdeal-integration/snapdeal-integration.component";
import { BackmarketIntegrationComponent } from "./backmarket-integration/backmarket-integration.component";
import { OpenchartEcommerceComponent } from "./openchart-ecommerce/openchart-ecommerce.component";
import { MagentoEcommerceComponent } from "./magento-ecommerce/magento-ecommerce.component";
import { PrestashopEcommerceComponent } from "./prestashop-ecommerce/prestashop-ecommerce.component";
import { ShopifyEcommerceComponent } from "./shopify-ecommerce/shopify-ecommerce.component";
import { BigEcommerceComponent } from "./big-ecommerce/big-ecommerce.component";
import { WooEcommerceComponent } from "./woo-ecommerce/woo-ecommerce.component";

/**
 * From Harsh Branch
 * Changed by: Aminul Aprijal
 * Date Modified: 22/11/2021 | 11:49 Indonesian Time
 */

 import { CreateorderComponent } from "./createorder/createorder.component";
 import { FindproductlistingerrorsComponent } from "./findproductlistingerrors/findproductlistingerrors.component";
 import { ShippingrulesComponent } from "./shippingrules/shippingrules.component";
 import { AddruleComponent } from "./addrule/addrule.component";
 import { EditinvoicetemplateComponent } from "./editinvoicetemplate/editinvoicetemplate.component";
 import { EditshippinglabelComponent } from "./editshippinglabel/editshippinglabel.component";
 import { EdittelesalechannelComponent } from "./edittelesalechannel/edittelesalechannel.component";
 import { EditownstorechannelComponent } from "./editownstorechannel/editownstorechannel.component";
 import { ChannelInfusionsoftAddComponent } from "./channel-infusionsoft-add/channel-infusionsoft-add.component";
 import { ChannelMailchimpAddComponent } from "./channel-mailchimp-add/channel-mailchimp-add.component";
 import { ChannelEposnowAddComponent } from "./channel-eposnow-add/channel-eposnow-add.component";
 import { AccountindexComponent } from "./accountindex/accountindex.component";
 import { ChannelQuickbooksAddComponent } from "./channel-quickbooks-add/channel-quickbooks-add.component";
 import { ChannelSageAddComponent } from "./channel-sage-add/channel-sage-add.component";
 import { ChannelKashflowAddComponent } from "./channel-kashflow-add/channel-kashflow-add.component";
 import { ChannelXeroAddComponent } from "./channel-xero-add/channel-xero-add.component";

export const ComponentsRoutes: Routes = [
    {
      path: '',
      children: [{
        path: 'buttons',
        component: ButtonsComponent
    }]}, {
    path: '',
    children: [{
      path: 'grid',
      component: GridSystemComponent
    }]
    }, {
      path: '',
      children: [{
        path: 'icons',
        component: IconsComponent
        }]
    }, {
        path: '',
        children: [{
            path: 'notifications',
            component: NotificationsComponent
        }]
    }, {
        path: '',
        children: [{
            path: 'panels',
            component: PanelsComponent
        }]
    }, {
        path: '',
        children: [{
            path: 'sweet-alert',
            component: SweetAlertComponent
        }]
    }, {
        path: '',
        children: [{
            path: 'typography',
            component: TypographyComponent
        }]
    },
    { 
        path: '',
            children: [{
                path: 'user-profile',
                component: UserProfileComponent
        }]
    },

    {
        path: '',
        children: [{
            path: 'tables', component: TablesComponent }]
    },
         {
        path: '',
        children: [{
            path: 'maps', component: MapsComponent }]
    },
         {
        path: '',
        children: [{
            path: 'products', component: ProductsComponent }]
    },
         {
        path: '',
        children: [{
            path: 'channelproducts', component: ChannelproductsComponent }]
    },
         {
        path: '',
        children: [{
            path: 'channelconnector', component: ChannelconnectorComponent }]
    },
         {
        path: '',
        children: [{
            path: 'stockview', component: StockviewComponent }]
    },
         {
        path: '',
        children: [{
            path: 'listingerror', component: ListingerrorComponent }]
    },
         {
        path: '',
        children: [{
            path: 'listedproduct', component: ListedproductComponent }]
    },
         {
        path: '',
        children: [{
            path: 'closedproduct', component: ClosedproductsComponent }]
    },
         {
        path: '',
        children: [{
            path: 'schedulelisting', component: SchedulelistingComponent }]
    },
         {
        path: '',
        children: [{
            path: 'ordersummary', component: OrdersummaryComponent }]
    },
         {
        path: '',
        children: [{
            path: 'mcforders', component: McfordersComponent }]
    },
         {
        path: '',
        children: [{
            path: 'shippedorders', component: ShippedordersComponent }]
    },
         {
        path: '',
        children: [{
            path: 'returnedorders', component: ReturnedordersComponent }]
    },
         {
        path: '',
        children: [{
            path: 'cancelledorders', component: CancelledordersComponent }]
    },
         {
        path: '',
        children: [{
            path: 'customers', component: CustomersComponent }]
    },
         {
        path: '',
        children: [{
            path: 'suppliers', component: SuppliersComponent }]
    },
         {
        path: '',
        children: [{
            path: 'newsuppliers', component: NewsuppliersComponent }]
    },
         {
        path: '',
        children: [{
            path: 'report-salesbyproducts', component: ReportSalesbyproductComponent }]
    },
         {
        path: '',
        children: [{
            path: 'report-lowstock', component: ReportLowstockComponent }]
    },
         {
        path: '',
        children: [{
            path: 'report-stockvalue', component: ReportStockvalueComponent }]
    },
         {
        path: '',
        children: [{
            path: 'report-orderhistory', component: ReportOrderhistoryComponent }]
    },
         {
        path: '',
        children: [{
            path: 'warehouse-transfer', component: WarehouseTransferComponent }]
    },
         {
        path: '',
        children: [{
            path: 'warehouse-stocksummary', component: WarehouseStocksummaryComponent }]
    },
         {
        path: '',
        children: [{
            path: 'warehouses', component: WarehousesComponent }]
    },
         {
        path: '',
        children: [{
            path: 'warehouse-add', component: WarehouseAddComponent }]
    },
         {
        path: '',
        children: [{
            path: 'manage-account', component: AccountManageaccountComponent }]
    },
         {
        path: '',
        children: [{
            path: 'manage-user', component: AccountManageuserComponent }]
    },
         {
        path: '',
        children: [{
            path: 'manage-subscriptions', component: AccountSubscriptionsComponent }]
    },
         {
        path: '',
        children: [{
            path: 'account-subscriptionsplan', component: AccountSubscriptionsplanComponent }]
    },
         {
        path: '',
        children: [{
            path: 'general-setting', component: SettingsGeneralsettingComponent }]
    },
         {
        path: '',
        children: [{
            path: 'product-attributes', component: SettingsProductattributesComponent }]
    },
         {
        path: '',
        children: [{
            path: 'bulk-actions', component: SettingsBulkactionsComponent }]
    },
         {
        path: '',
        children: [{
            path: 'barcode-management', component: SettingsBarcodemanagementComponent }]
    },
         {
        path: '',
        children: [{
            path: 'mail-setting', component: SettingsMailsettingComponent }]
    },
         {
        path: '',
        children: [{
            path: 'notification-setting', component: SettingsNotificationsettingComponent }]
    },
         {
        path: '',
        children: [{
            path: 'inventory-synchronize', component: SettingsInventorysycComponent }]
    },
         {
        path: '',
        children: [{
            path: 'invoice-shiping-label', component: SettingsInvoiceComponent }]
    },
         {
        path: '',
        children: [{
            path: 'about-message', component: AboutMessagesComponent }]
    },
         {
        path: '',
        children: [{
            path: 'sales-orders', component: SalesOrdersComponent }]
    },
         {
        path: '',
        children: [{
            path: 'fab-createproducts', component: FabCreateproductsComponent }]
    },
         {
        path: '',
        children: [{
            path: 'fab-sendtofba', component: FabSendtofbaComponent }]
    },
         {
        path: '',
        children: [{
            path: 'fab-productinfba', component: FabProductinfbaComponent }]
    },
         {
        path: '',
        children: [{
            path: 'purchaseorder', component: PurchaseorderComponent }]
    },
         {
        path: '',
        children: [{
            path: 'managereturn', component: ManageReturnComponent }]
    },
         {
        path: '',
        children: [{
            path: 'waitingtolist', component: WaitingtolistComponent }]
    },
         {
        path: '',
        children: [{
            path: 'shippingcourierlist', component: ShippingcourierlistComponent }]
    },
         {
        path: '',
        children: [{
            path: 'bluedart', component:BluedartComponent }]
    },
         {
        path: '',
        children: [{
            path: 'easyparcel', component: EasyparcelComponent }]
    },
         {
        path: '',
        children: [{
            path: 'dpd', component: DpdintegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'royalmailsetting', component: RoyalmailsettingsComponent }]
    },
         {
        path: '',
        children: [{
            path: 'royalmaillabelsetup', component: RoyalmailslabelsetupComponent }]
    },
         {
        path: '',
        children: [{
            path: 'royalmailppilist', component: RoyalmailppilistComponent }]
    },
         {
        path: '',
        children: [{
            path: 'royalmaildmolist', component: RoyalmaildmolistComponent }]
    },
         {
        path: '',
        children: [{
            path: 'royalmailintereq', component: RoyalmailintereqComponent }]
    },
         {
        path: '',
        children: [{
            path: 'royalmailchannelmap', component: RoyalmailchannelmapComponent }]
    },
         {
        path: '',
        children: [{
            path: 'shipstationlabelsetup', component: ShipstationlabelsetupComponent }]
    },
         {
        path: '',
        children: [{
            path: 'hermessettings', component: HermessettingsComponent }]
    },
         {
        path: '',
        children: [{
            path: 'myparceldelivery', component: MyparceldeliveryComponent }]
    },
         {
        path: '',
        children: [{
            path: 'springglobalmail', component: SpringglobalmailComponent}]
    },
         {
        path: '',
        children: [{
            path: 'wndirect', component: WndirectComponent }]
    },
         {
        path: '',
        children: [{
            path: 'dxlabelsetup', component: DxlabelsetupComponent}]
    },
         {
        path: '',
        children: [{
            path: 'clickdroplabelsetup', component: ClickdroplabelsetupComponent }]
    },
         {
        path: '',
        children: [{
            path: 'webshipperlabelsetup', component: WebshipperlabelsetupComponent }]
    },
         {
        path: '',
        children: [{
            path: 'ukmailsettings', component: UkmailsettingsComponent }]
    },
         {
        path: '',
        children: [{
            path: 'ukmaillabelsetup', component: UkmaillabelsetupComponent }]
    },
         {
        path: '',
        children: [{
            path: 'yodellabelsetup', component: YodellabelsetupComponent }]
    },
         {
        path: '',
        children: [{
            path: 'shippolabelsetup', component: ShippolabelsetupComponent }]
    },
         {
        path: '',
        children: [{
            path: 'parcelmonkeylabelsetup', component: ParcelmonkeylabelsetupComponent }]
    },
         {
        path: '',
        children: [{
            path: 'fedexlabelsetup', component: FedexlabelsetupComponent }]
    },
         {
        path: '',
        children: [{
            path: 'interlinklabelsetup',       component: InterlinklabelsetupComponent }]
    },
         {
        path: '',
        children: [{
            path: 'purchaseorderadd',          component: PurchaseorderaddComponent }]
    },
         {
        path: '',
        children: [{
            path: 'create-product-to-fba',     component: CreateProductsTofbaComponent }]
    },
         {
        path: '',
        children: [{
            path: 'invoicetemplates',          component: InvoicetemplatesComponent }]
    },
         {
        path: '',
        children: [{
            path: 'invoice',                   component: InvoiceComponent }]
    },
         {
        path: '',
        children: [{
            path: 'shippinglabel',             component: ShippinglabelComponent }]
    },
         {
        path: '',
        children: [{
            path: 'pickuplist',                component: PickuplistComponent }]
    },
         {
        path: '',
        children: [{
            path: 'packlist',                  component: PacklistComponent }]
    },
         {
        path: '',
        children: [{
            path: 'whatsnew',                  component: WhatsnewComponent }]
    },
         {
        path: '',
        children: [{
            path: 'add-new-products',          component: AddnewproductsComponent }]
    },
         {
        path: '',
        children: [{
            path: 'ebay-channel-integration',  component: EbayIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'rakuten-channel-integration', component: RakutenIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'sears-channel-integration', component: SearsIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'cdiscount-channel-integration', component: CdiscountIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'cdiscount-listing-templates', component: CdiscountListingTemplatesComponent }]
    },
         {
        path: '',
        children: [{
            path: 'priceminister-channel-integration', component: PriceministerIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'bonza-channel-integration', component: BonzaIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'flubit-channel-integration', component: FlubitIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'etsy-channel-integration', component: EtsyIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'jet-channel-integration', component: JetIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'frugo-channel-integration', component: FrugoIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'ioffer-channel-integration', component: IofferIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'storenvy-channel-integration', component: StorenvyIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'opensky-channel-integration', component: OpenskyIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'lazada-channel-integration', component: LazadaIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'lazada-template-channel-integration', component: LazadaTemplateIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'warlmart-template-channel-integration', component: WalmartIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'bolcom-template-channel-integration', component: BolcomIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'onbuy-template-channel-integration', component: OnbuyIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'tesco-template-channel-integration', component: TescoIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'shopclues-template-channel-integration', component: ShopcluesIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'flipkart-template-channel-integration', component: FlipkartIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'google-shopping-channel-integration', component: GoogleshopingIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'shopee-channel-integration', component: ShopeeIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'lelong-channel-integration', component: LelongIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'street-channel-integration', component: StreetIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'snapdeal-channel-integration', component: SnapdealIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'backmarket-channel-integration', component: BackmarketIntegrationComponent }]
    },
         {
        path: '',
        children: [{
            path: 'openchart-ecommerce', component: OpenchartEcommerceComponent }]
    },
         {
        path: '',
        children: [{
            path: 'magento-ecommerce', component: MagentoEcommerceComponent }]
    },
         {
        path: '',
        children: [{
            path: 'prestashop-ecommerce', component: PrestashopEcommerceComponent }]
    },
         {
        path: '',
        children: [{
            path: 'shopify-ecommerce', component: ShopifyEcommerceComponent }]
    },
         {
        path: '',
        children: [{
            path: 'big-ecommerce', component: BigEcommerceComponent }]
    },
         {
        path: '',
        children: [{
            path: 'woo-ecommerce', component: WooEcommerceComponent }]
    },

         /**
          * From Harsh Branch
          */

          {
            path: '',
            children: [{
                path: 'createorder',
                component: CreateorderComponent 
            }]
          },
          {
            path: '',
            children: [{
                path: 'findproductlistingerrors',  
                component: FindproductlistingerrorsComponent 
            }]
           },
           {
            path: '',
            children: [{
                path: 'shippingrules',             
                component: ShippingrulesComponent 
            }]
           },
           {
            path: '',
            children: [{
                path: 'addrule',                   
                component: AddruleComponent 
            }]
          },
          {
            path: '',
            children: [{
                path: 'editinvoicetemplate',       
                component: EditinvoicetemplateComponent 
            }]
           },
           {
            path: '',
            children: [{
                path: 'editshippinglabel',         
                component: EditshippinglabelComponent 
            }]
          },
          {
            path: '',
            children: [{
                path: 'channel-other-add',         
                component: EdittelesalechannelComponent 
            }]
          },
          {
            path: '',
            children: [{
                path: 'channel-other-add-new',     
                component: EditownstorechannelComponent 
            }]
          },
          {
            path: '',
            children: [{
                path: 'channel-infusionsoft-add',  
                component: ChannelInfusionsoftAddComponent 
            }]
          },
          {
            path: '',
            children: [{
                path: 'channel-mailchimp-add',     
                component: ChannelMailchimpAddComponent 
            }]
          },
          {
            path: '',
            children: [{
                path: 'channel-eposnow-add',       
                component: ChannelEposnowAddComponent 
            }]
          },
          {
            path: '',
            children: [{
                path: 'accountindex',              
                component: AccountindexComponent 
            }]
          },
          {
            path: '',
            children: [{
                path: 'channel-quickbooks-add',    
                component: ChannelQuickbooksAddComponent 
            }]
          },
          {
            path: '',
            children: [{
                path: 'channel-sage-add',          
                component: ChannelSageAddComponent 
            }]
          },
              {
            path: '',
            children: [{
                path: 'channel-kashflow-add',      
                component: ChannelKashflowAddComponent 
            }]
          },
          {
            path: '',
            children: [{
                path: 'channel-xero-add',          
                component: ChannelXeroAddComponent 
            }]
          }

         /**
          * EOF Harsh Branch
          */
];
