import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateProductsTofbaComponent } from './create-products-tofba.component';

describe('CreateProductsTofbaComponent', () => {
  let component: CreateProductsTofbaComponent;
  let fixture: ComponentFixture<CreateProductsTofbaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateProductsTofbaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateProductsTofbaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
