import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-create-products-tofba',
  templateUrl: './create-products-tofba.component.html',
  styleUrls: ['./create-products-tofba.component.css']
})
export class CreateProductsTofbaComponent implements OnInit {
  listTemplate = true;
  cost_price: number;
  selling_price: number;
  constructor() {}
  ngOnInit(): void {
    this.cost_price = 2000;
    this.selling_price = 2200;
  }

  showNewListingTemplates(){
    this.listTemplate = false;
    
  }

}
