import { Component, OnInit, ViewChild } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})

export class CustomersComponent implements OnInit {
  email: string; 
  firstname: string; 
  secondname: string; 
  addressone: string; 
  addresstwo: string;
  addressthree: string;
  notes: string;
  city: string;
  state: string;
  postcode: number;
  country: string;
  priority: string

  dataCustomer;
  page=1;
  pageSize=10;
  pageSizeSelect;
  txtSearch: string;
  @ViewChild('closebutton') closebutton;
  sortData: string = 'priority';
  
  constructor() { 
    this.loadDataCustomer();
  }

  ngOnInit(): void {
    this.pageSizeSelect = this.pageSize;
  }

  saveCustomer() {

    var isExistData = JSON.parse(localStorage.getItem("AllCustomerData"));
    if(isExistData == null) isExistData = [];

    let JSONCustomer = {
        "email": this.email, 
        "firstname": this.firstname,
        "secondname": this.secondname, 
        "addressone": this.addressone, 
        "addresstwo": this.addresstwo, 
        "addressthree": this.addressthree, 
        "notes": this.notes, 
        "city": this.city, 
        "state": this.state, 
        "postcode": this.postcode, 
        "country": this.country, 
        "priority": this.priority
    }

    localStorage.setItem("CustomerData", JSON.stringify(JSONCustomer));
    isExistData.push(JSONCustomer);
    localStorage.setItem("AllCustomerData", JSON.stringify(isExistData));

    Swal.fire({
      title: 'Success',
      text: 'New customer successfull adding',
      icon: 'success'
    });
    this.loadDataCustomer();
    this.closebutton.nativeElement.click();
  }

  loadDataCustomer() {
    this.dataCustomer = JSON.parse(localStorage.getItem("AllCustomerData"));
    console.log(this.dataCustomer);
  }

  
  searchCustomer() {
    let arr = this.dataCustomer;
    this.dataCustomer = arr.filter(a => a.txtSearch);
    console.log(this.dataCustomer);
  }
  
  pageSizeChange(event){
    this.pageSizeSelect = event;
  }

  key: string = this.sortData;
  sortBy(key){
    this.key = key;
  }
}
