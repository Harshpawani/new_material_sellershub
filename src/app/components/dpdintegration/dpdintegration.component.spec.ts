import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DpdintegrationComponent } from './dpdintegration.component';

describe('DpdintegrationComponent', () => {
  let component: DpdintegrationComponent;
  let fixture: ComponentFixture<DpdintegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DpdintegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DpdintegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
