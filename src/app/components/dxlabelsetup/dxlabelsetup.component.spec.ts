import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DxlabelsetupComponent } from './dxlabelsetup.component';

describe('DxlabelsetupComponent', () => {
  let component: DxlabelsetupComponent;
  let fixture: ComponentFixture<DxlabelsetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DxlabelsetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DxlabelsetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
