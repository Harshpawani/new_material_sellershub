import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EasyparcelComponent } from './easyparcel.component';

describe('EasyparcelComponent', () => {
  let component: EasyparcelComponent;
  let fixture: ComponentFixture<EasyparcelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EasyparcelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EasyparcelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
