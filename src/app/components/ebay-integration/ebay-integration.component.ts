import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ebay-integration',
  templateUrl: './ebay-integration.component.html',
  styleUrls: ['./ebay-integration.component.css']
})
export class EbayIntegrationComponent implements OnInit {

  logo: string;
  listTemplate = true;
  cost_price: number;
  selling_price: number;
  constructor(private router: Router) { 
    this.logo = this.router.getCurrentNavigation().extras.state.logo;
    this.cost_price = 2000;
    this.selling_price = 2200;
  }

  ngOnInit(): void {
  }

  showNewListingTemplates(){
    this.listTemplate = false; 
  }

}
