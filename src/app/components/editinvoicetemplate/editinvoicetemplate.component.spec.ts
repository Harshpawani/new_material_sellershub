import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditinvoicetemplateComponent } from './editinvoicetemplate.component';

describe('EditinvoicetemplateComponent', () => {
  let component: EditinvoicetemplateComponent;
  let fixture: ComponentFixture<EditinvoicetemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditinvoicetemplateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditinvoicetemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
