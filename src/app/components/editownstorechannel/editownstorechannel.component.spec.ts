import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditownstorechannelComponent } from './editownstorechannel.component';

describe('EditownstorechannelComponent', () => {
  let component: EditownstorechannelComponent;
  let fixture: ComponentFixture<EditownstorechannelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditownstorechannelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditownstorechannelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
