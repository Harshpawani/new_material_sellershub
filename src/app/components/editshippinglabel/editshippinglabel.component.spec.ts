import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditshippinglabelComponent } from './editshippinglabel.component';

describe('EditshippinglabelComponent', () => {
  let component: EditshippinglabelComponent;
  let fixture: ComponentFixture<EditshippinglabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditshippinglabelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditshippinglabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
