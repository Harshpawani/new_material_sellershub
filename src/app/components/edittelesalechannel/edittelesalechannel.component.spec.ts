import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdittelesalechannelComponent } from './edittelesalechannel.component';

describe('EdittelesalechannelComponent', () => {
  let component: EdittelesalechannelComponent;
  let fixture: ComponentFixture<EdittelesalechannelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdittelesalechannelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdittelesalechannelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
