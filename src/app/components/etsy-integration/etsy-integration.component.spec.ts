import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtsyIntegrationComponent } from './etsy-integration.component';

describe('EtsyIntegrationComponent', () => {
  let component: EtsyIntegrationComponent;
  let fixture: ComponentFixture<EtsyIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtsyIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtsyIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
