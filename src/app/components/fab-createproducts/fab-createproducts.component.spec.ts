import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FabCreateproductsComponent } from './fab-createproducts.component';

describe('FabCreateproductsComponent', () => {
  let component: FabCreateproductsComponent;
  let fixture: ComponentFixture<FabCreateproductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FabCreateproductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FabCreateproductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
