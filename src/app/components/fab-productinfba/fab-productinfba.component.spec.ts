import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FabProductinfbaComponent } from './fab-productinfba.component';

describe('FabProductinfbaComponent', () => {
  let component: FabProductinfbaComponent;
  let fixture: ComponentFixture<FabProductinfbaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FabProductinfbaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FabProductinfbaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
