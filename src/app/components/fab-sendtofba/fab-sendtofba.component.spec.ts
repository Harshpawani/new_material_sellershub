import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FabSendtofbaComponent } from './fab-sendtofba.component';

describe('FabSendtofbaComponent', () => {
  let component: FabSendtofbaComponent;
  let fixture: ComponentFixture<FabSendtofbaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FabSendtofbaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FabSendtofbaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
