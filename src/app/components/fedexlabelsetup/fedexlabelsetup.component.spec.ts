import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FedexlabelsetupComponent } from './fedexlabelsetup.component';

describe('FedexlabelsetupComponent', () => {
  let component: FedexlabelsetupComponent;
  let fixture: ComponentFixture<FedexlabelsetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FedexlabelsetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FedexlabelsetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
