import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FindproductlistingerrorsComponent } from './findproductlistingerrors.component';

describe('FindproductlistingerrorsComponent', () => {
  let component: FindproductlistingerrorsComponent;
  let fixture: ComponentFixture<FindproductlistingerrorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FindproductlistingerrorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FindproductlistingerrorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
