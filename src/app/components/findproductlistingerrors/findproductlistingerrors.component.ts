import { Component, OnInit } from '@angular/core';
import Swal from "sweetalert2";

@Component({
  selector: "app-findproductlistingerrors",
  templateUrl: "./findproductlistingerrors.component.html",
  styleUrls: ["./findproductlistingerrors.component.css"],
})
export class FindproductlistingerrorsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  switchtabOne() {
    document.getElementById("theone").className = "nav-link active";
    document.getElementById("thetwo").className = "nav-link";
    document.getElementById("one").className = "tab-pane active";
    document.getElementById("two").className = "tab-pane fade";
  }

  switchtabTwo() {
    document.getElementById("theone").className = "nav-link";
    document.getElementById("thetwo").className = "nav-link active";
    document.getElementById("one").className = "tab-pane fade";
    document.getElementById("two").className = "tab-pane active";
  }

  switchtabThree() {
    document.getElementById("thetwo").className = "nav-link";
    document.getElementById("thethree").className = "nav-link active";
    document.getElementById("two").className = "tab-pane fade";
    document.getElementById("three").className = "tab-pane active";
  }

  switchtabTwoFromThree() {
    document.getElementById("thetwo").className = "nav-link active";
    document.getElementById("two").className = "tab-pane active";
    document.getElementById("thethree").className = "nav-link";
    document.getElementById("three").className = "tab-pane";
  }

  switchtabFour() {
    document.getElementById("thethree").className = "nav-link";
    document.getElementById("three").className = "tab-pane";
    document.getElementById("thefour").className = "nav-link active";
    document.getElementById("four").className = "tab-pane active";
  }

  switchtabFromFour() {
    document.getElementById("three").className = "tab-pane active";
    document.getElementById("thethree").className = "nav-link active";
    document.getElementById("thefour").className = "nav-link";
    document.getElementById("four").className = "tab-pane fade";
  }

  switchtabFive() {
    document.getElementById("thefour").className = "nav-link";
    document.getElementById("four").className = "tab-pane fade";
    document.getElementById("thefive").className = "nav-link active";
    document.getElementById("five").className = "tab-pane active";
  }

  switchtabFromFive() {
    document.getElementById("thefour").className = "nav-link active";
    document.getElementById("four").className = "tab-pane active";
    document.getElementById("thefive").className = "nav-link";
    document.getElementById("five").className = "tab-pane fade";
  }

  saveProduct() {
    Swal.fire({
      title: "Success",
      text: "Product successfull to add",
      icon: "success",
    });
  }
}
