import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlipkartIntegrationComponent } from './flipkart-integration.component';

describe('FlipkartIntegrationComponent', () => {
  let component: FlipkartIntegrationComponent;
  let fixture: ComponentFixture<FlipkartIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlipkartIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlipkartIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
