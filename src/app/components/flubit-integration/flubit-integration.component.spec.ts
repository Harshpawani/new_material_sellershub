import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlubitIntegrationComponent } from './flubit-integration.component';

describe('FlubitIntegrationComponent', () => {
  let component: FlubitIntegrationComponent;
  let fixture: ComponentFixture<FlubitIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlubitIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlubitIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
