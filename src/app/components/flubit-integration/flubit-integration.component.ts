import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-flubit-integration',
  templateUrl: './flubit-integration.component.html',
  styleUrls: ['./flubit-integration.component.css']
})
export class FlubitIntegrationComponent implements OnInit {

  title: string;
  logo: string;
  constructor(private router: Router) { 
    this.logo = this.router.getCurrentNavigation().extras.state.logo;
    this.title = this.router.getCurrentNavigation().extras.state.title;
  }

  ngOnInit(): void {
  }

}
