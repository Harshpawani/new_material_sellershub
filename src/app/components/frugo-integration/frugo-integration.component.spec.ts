import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FrugoIntegrationComponent } from './frugo-integration.component';

describe('FrugoIntegrationComponent', () => {
  let component: FrugoIntegrationComponent;
  let fixture: ComponentFixture<FrugoIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FrugoIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FrugoIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
