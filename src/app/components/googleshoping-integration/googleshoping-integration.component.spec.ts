import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoogleshopingIntegrationComponent } from './googleshoping-integration.component';

describe('GoogleshopingIntegrationComponent', () => {
  let component: GoogleshopingIntegrationComponent;
  let fixture: ComponentFixture<GoogleshopingIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GoogleshopingIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GoogleshopingIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
