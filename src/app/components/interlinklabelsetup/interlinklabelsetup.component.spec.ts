import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InterlinklabelsetupComponent } from './interlinklabelsetup.component';

describe('InterlinklabelsetupComponent', () => {
  let component: InterlinklabelsetupComponent;
  let fixture: ComponentFixture<InterlinklabelsetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InterlinklabelsetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InterlinklabelsetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
