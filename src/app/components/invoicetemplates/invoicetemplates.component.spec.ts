import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicetemplatesComponent } from './invoicetemplates.component';

describe('InvoicetemplatesComponent', () => {
  let component: InvoicetemplatesComponent;
  let fixture: ComponentFixture<InvoicetemplatesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InvoicetemplatesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicetemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
