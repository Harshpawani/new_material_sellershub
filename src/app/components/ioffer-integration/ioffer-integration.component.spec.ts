import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IofferIntegrationComponent } from './ioffer-integration.component';

describe('IofferIntegrationComponent', () => {
  let component: IofferIntegrationComponent;
  let fixture: ComponentFixture<IofferIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IofferIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IofferIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
