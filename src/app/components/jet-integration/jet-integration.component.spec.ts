import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JetIntegrationComponent } from './jet-integration.component';

describe('JetIntegrationComponent', () => {
  let component: JetIntegrationComponent;
  let fixture: ComponentFixture<JetIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JetIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JetIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
