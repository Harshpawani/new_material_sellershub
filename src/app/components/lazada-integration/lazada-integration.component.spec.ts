import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LazadaIntegrationComponent } from './lazada-integration.component';

describe('LazadaIntegrationComponent', () => {
  let component: LazadaIntegrationComponent;
  let fixture: ComponentFixture<LazadaIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LazadaIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LazadaIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
