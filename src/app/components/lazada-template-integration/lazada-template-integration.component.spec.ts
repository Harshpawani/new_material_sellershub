import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LazadaTemplateIntegrationComponent } from './lazada-template-integration.component';

describe('LazadaTemplateIntegrationComponent', () => {
  let component: LazadaTemplateIntegrationComponent;
  let fixture: ComponentFixture<LazadaTemplateIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LazadaTemplateIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LazadaTemplateIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
