import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-lazada-template-integration',
  templateUrl: './lazada-template-integration.component.html',
  styleUrls: ['./lazada-template-integration.component.css']
})
export class LazadaTemplateIntegrationComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  switchtabOne() {
    document.getElementById("theone").className = "nav-link active";
    document.getElementById("thetwo").className = "nav-link";
    document.getElementById("one").className = "tab-pane active";
    document.getElementById("two").className = "tab-pane fade";
  }

  switchtabTwo() {
    document.getElementById("theone").className = "nav-link";
    document.getElementById("thetwo").className = "nav-link active";
    document.getElementById("one").className = "tab-pane fade";
    document.getElementById("two").className = "tab-pane active";
  }


  saveProduct() {
    Swal.fire({
      title: 'Success',
      text: 'Successfull to add',
      icon: 'success'
    });
  }
}
