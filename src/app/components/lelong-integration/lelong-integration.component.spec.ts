import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LelongIntegrationComponent } from './lelong-integration.component';

describe('LelongIntegrationComponent', () => {
  let component: LelongIntegrationComponent;
  let fixture: ComponentFixture<LelongIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LelongIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LelongIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
