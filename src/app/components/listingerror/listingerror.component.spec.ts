import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListingerrorComponent } from './listingerror.component';

describe('ListingerrorComponent', () => {
  let component: ListingerrorComponent;
  let fixture: ComponentFixture<ListingerrorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListingerrorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingerrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
