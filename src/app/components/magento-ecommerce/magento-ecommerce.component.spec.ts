import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MagentoEcommerceComponent } from './magento-ecommerce.component';

describe('MagentoEcommerceComponent', () => {
  let component: MagentoEcommerceComponent;
  let fixture: ComponentFixture<MagentoEcommerceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MagentoEcommerceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MagentoEcommerceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
