import { ComponentFixture, TestBed } from '@angular/core/testing';

import { McfordersComponent } from './mcforders.component';

describe('McfordersComponent', () => {
  let component: McfordersComponent;
  let fixture: ComponentFixture<McfordersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ McfordersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(McfordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
