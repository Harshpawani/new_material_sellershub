import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mcforders',
  templateUrl: './mcforders.component.html',
  styleUrls: ['./mcforders.component.css']
})
export class McfordersComponent implements OnInit {

  menuData;
  constructor(private router: Router) {
    this.getMenu();
  }

  ngOnInit(): void {
    this.getMenu();
  }

  getMenu() {
    this.menuData = JSON.parse(localStorage.getItem("AllTabMenu"));
    console.log(this.menuData);
  }

  removeMenu(menuTitle, menuLink){
    var dataUser = JSON.parse(localStorage.getItem("AllTabMenu"));
        var index = dataUser.findIndex(item => item.menuLink === menuLink);
        console.log('Index: ' + index);
        dataUser.splice(index, 1);
        localStorage.setItem("AllTabMenu", JSON.stringify(dataUser));
        this.getMenu();
  }

  openMenu(menuLink) {
    this.router.navigateByUrl(menuLink);
  }

}
