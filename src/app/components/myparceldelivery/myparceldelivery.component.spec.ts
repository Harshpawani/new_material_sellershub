import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyparceldeliveryComponent } from './myparceldelivery.component';

describe('MyparceldeliveryComponent', () => {
  let component: MyparceldeliveryComponent;
  let fixture: ComponentFixture<MyparceldeliveryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyparceldeliveryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyparceldeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
