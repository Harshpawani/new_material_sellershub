import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsuppliersComponent } from './newsuppliers.component';

describe('NewsuppliersComponent', () => {
  let component: NewsuppliersComponent;
  let fixture: ComponentFixture<NewsuppliersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsuppliersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsuppliersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
