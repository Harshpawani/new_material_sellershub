import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnbuyIntegrationComponent } from './onbuy-integration.component';

describe('OnbuyIntegrationComponent', () => {
  let component: OnbuyIntegrationComponent;
  let fixture: ComponentFixture<OnbuyIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnbuyIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnbuyIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
