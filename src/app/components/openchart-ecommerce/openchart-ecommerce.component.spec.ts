import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenchartEcommerceComponent } from './openchart-ecommerce.component';

describe('OpenchartEcommerceComponent', () => {
  let component: OpenchartEcommerceComponent;
  let fixture: ComponentFixture<OpenchartEcommerceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpenchartEcommerceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenchartEcommerceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
