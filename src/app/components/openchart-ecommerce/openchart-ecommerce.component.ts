import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-openchart-ecommerce',
  templateUrl: './openchart-ecommerce.component.html',
  styleUrls: ['./openchart-ecommerce.component.css']
})
export class OpenchartEcommerceComponent implements OnInit {

  titleEcommrce: string;
  logoEcommerce: string;
  constructor(private router: Router) { 
    this.logoEcommerce = this.router.getCurrentNavigation().extras.state.logo;
    this.titleEcommrce = this.router.getCurrentNavigation().extras.state.title;
    console.log("Title From Eco: " + this.titleEcommrce);
  }

  ngOnInit(): void {
  }

}
