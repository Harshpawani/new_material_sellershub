import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenskyIntegrationComponent } from './opensky-integration.component';

describe('OpenskyIntegrationComponent', () => {
  let component: OpenskyIntegrationComponent;
  let fixture: ComponentFixture<OpenskyIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpenskyIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenskyIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
