import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ordersummary',
  templateUrl: './ordersummary.component.html',
  styleUrls: ['./ordersummary.component.css']
})
export class OrdersummaryComponent implements OnInit {
  menuData;
  constructor(private router: Router) { 
    this.getMenu();
  }

  ngOnInit(): void {
    this.getMenu();
  }

  closeOne() {
    alert("Tes");
  }

  getMenu() {
    this.menuData = JSON.parse(localStorage.getItem("AllTabMenu"));
    console.log(this.menuData);
  }

  removeMenu(menuTitle, menuLink){
    var dataUser = JSON.parse(localStorage.getItem("AllTabMenu"));
        var index = dataUser.findIndex(item => item.menuLink === menuLink);
        console.log('Index: ' + index);
        dataUser.splice(index, 1);
        localStorage.setItem("AllTabMenu", JSON.stringify(dataUser));
        this.getMenu();
  }

  openMenu(menuLink) {
    this.router.navigateByUrl(menuLink);
  }

}
