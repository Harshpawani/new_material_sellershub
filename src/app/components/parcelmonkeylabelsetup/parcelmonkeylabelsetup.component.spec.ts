import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParcelmonkeylabelsetupComponent } from './parcelmonkeylabelsetup.component';

describe('ParcelmonkeylabelsetupComponent', () => {
  let component: ParcelmonkeylabelsetupComponent;
  let fixture: ComponentFixture<ParcelmonkeylabelsetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParcelmonkeylabelsetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParcelmonkeylabelsetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
