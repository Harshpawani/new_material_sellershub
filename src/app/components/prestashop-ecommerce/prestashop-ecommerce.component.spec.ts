import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrestashopEcommerceComponent } from './prestashop-ecommerce.component';

describe('PrestashopEcommerceComponent', () => {
  let component: PrestashopEcommerceComponent;
  let fixture: ComponentFixture<PrestashopEcommerceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrestashopEcommerceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrestashopEcommerceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
