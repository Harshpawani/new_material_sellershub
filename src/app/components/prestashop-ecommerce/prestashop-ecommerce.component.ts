import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-prestashop-ecommerce',
  templateUrl: './prestashop-ecommerce.component.html',
  styleUrls: ['./prestashop-ecommerce.component.css']
})
export class PrestashopEcommerceComponent implements OnInit {

  titleEcommrce: string;
  logoEcommerce: string;
  constructor(private router: Router) { 
    this.logoEcommerce = this.router.getCurrentNavigation().extras.state.logo;
    this.titleEcommrce = this.router.getCurrentNavigation().extras.state.title;
    console.log("Title From Eco: " + this.titleEcommrce);
  }

  ngOnInit(): void {
  }

}
