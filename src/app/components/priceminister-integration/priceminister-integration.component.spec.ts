import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PriceministerIntegrationComponent } from './priceminister-integration.component';

describe('PriceministerIntegrationComponent', () => {
  let component: PriceministerIntegrationComponent;
  let fixture: ComponentFixture<PriceministerIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PriceministerIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceministerIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
