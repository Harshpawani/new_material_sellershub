import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-priceminister-integration',
  templateUrl: './priceminister-integration.component.html',
  styleUrls: ['./priceminister-integration.component.css']
})
export class PriceministerIntegrationComponent implements OnInit {

  logo: string;
  cost_price: number;
  selling_price: number;
  constructor(private router: Router) {
    this.logo = this.router.getCurrentNavigation().extras.state.logo;
    this.cost_price = 2000;
    this.selling_price = 2200;
   }

  ngOnInit(): void {
  }

}
