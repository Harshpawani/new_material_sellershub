import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  isChecked = false;
  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  gotoAddNewProduct() {
    this.router.navigateByUrl('/add-new-products');
  }

  alert(){
    if(this.isChecked == true){
      alert("Selected");
    } else{
      alert("Select at least one product to link.");
    }
  }
}
