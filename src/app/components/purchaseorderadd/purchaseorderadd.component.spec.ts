import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseorderaddComponent } from './purchaseorderadd.component';

describe('PurchaseorderaddComponent', () => {
  let component: PurchaseorderaddComponent;
  let fixture: ComponentFixture<PurchaseorderaddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchaseorderaddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseorderaddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
