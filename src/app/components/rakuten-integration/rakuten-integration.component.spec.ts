import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RakutenIntegrationComponent } from './rakuten-integration.component';

describe('RakutenIntegrationComponent', () => {
  let component: RakutenIntegrationComponent;
  let fixture: ComponentFixture<RakutenIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RakutenIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RakutenIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
