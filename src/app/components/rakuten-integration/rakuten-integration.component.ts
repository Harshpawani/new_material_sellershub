import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-rakuten-integration',
  templateUrl: './rakuten-integration.component.html',
  styleUrls: ['./rakuten-integration.component.css']
})
export class RakutenIntegrationComponent implements OnInit {
  logo: string;
  cost_price: number;
  selling_price: number;
  constructor(private router: Router) {
    this.logo = this.router.getCurrentNavigation().extras.state.logo;
    this.cost_price = 2000;
    this.selling_price = 2200;
   }

  ngOnInit(): void {
  }

}
