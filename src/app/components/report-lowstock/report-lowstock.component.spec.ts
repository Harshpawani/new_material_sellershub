import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportLowstockComponent } from './report-lowstock.component';

describe('ReportLowstockComponent', () => {
  let component: ReportLowstockComponent;
  let fixture: ComponentFixture<ReportLowstockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportLowstockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportLowstockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
