import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportOrderhistoryComponent } from './report-orderhistory.component';

describe('ReportOrderhistoryComponent', () => {
  let component: ReportOrderhistoryComponent;
  let fixture: ComponentFixture<ReportOrderhistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportOrderhistoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportOrderhistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
