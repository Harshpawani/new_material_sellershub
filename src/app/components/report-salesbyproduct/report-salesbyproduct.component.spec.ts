import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportSalesbyproductComponent } from './report-salesbyproduct.component';

describe('ReportSalesbyproductComponent', () => {
  let component: ReportSalesbyproductComponent;
  let fixture: ComponentFixture<ReportSalesbyproductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportSalesbyproductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportSalesbyproductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
