import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportStockvalueComponent } from './report-stockvalue.component';

describe('ReportStockvalueComponent', () => {
  let component: ReportStockvalueComponent;
  let fixture: ComponentFixture<ReportStockvalueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportStockvalueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportStockvalueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
