import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReturnedordersComponent } from './returnedorders.component';

describe('ReturnedordersComponent', () => {
  let component: ReturnedordersComponent;
  let fixture: ComponentFixture<ReturnedordersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReturnedordersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReturnedordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
