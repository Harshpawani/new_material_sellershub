import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoyalmailchannelmapComponent } from './royalmailchannelmap.component';

describe('RoyalmailchannelmapComponent', () => {
  let component: RoyalmailchannelmapComponent;
  let fixture: ComponentFixture<RoyalmailchannelmapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoyalmailchannelmapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoyalmailchannelmapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
