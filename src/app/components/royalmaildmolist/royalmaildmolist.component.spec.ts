import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoyalmaildmolistComponent } from './royalmaildmolist.component';

describe('RoyalmaildmolistComponent', () => {
  let component: RoyalmaildmolistComponent;
  let fixture: ComponentFixture<RoyalmaildmolistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoyalmaildmolistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoyalmaildmolistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
