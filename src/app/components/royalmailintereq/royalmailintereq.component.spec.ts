import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoyalmailintereqComponent } from './royalmailintereq.component';

describe('RoyalmailintereqComponent', () => {
  let component: RoyalmailintereqComponent;
  let fixture: ComponentFixture<RoyalmailintereqComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoyalmailintereqComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoyalmailintereqComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
