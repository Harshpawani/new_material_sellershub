import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoyalmailppilistComponent } from './royalmailppilist.component';

describe('RoyalmailppilistComponent', () => {
  let component: RoyalmailppilistComponent;
  let fixture: ComponentFixture<RoyalmailppilistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoyalmailppilistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoyalmailppilistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
