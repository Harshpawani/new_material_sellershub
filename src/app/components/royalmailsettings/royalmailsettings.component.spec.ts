import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoyalmailsettingsComponent } from './royalmailsettings.component';

describe('RoyalmailsettingsComponent', () => {
  let component: RoyalmailsettingsComponent;
  let fixture: ComponentFixture<RoyalmailsettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoyalmailsettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoyalmailsettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
