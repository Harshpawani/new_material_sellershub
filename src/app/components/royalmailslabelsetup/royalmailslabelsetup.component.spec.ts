import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoyalmailslabelsetupComponent } from './royalmailslabelsetup.component';

describe('RoyalmailslabelsetupComponent', () => {
  let component: RoyalmailslabelsetupComponent;
  let fixture: ComponentFixture<RoyalmailslabelsetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoyalmailslabelsetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoyalmailslabelsetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
