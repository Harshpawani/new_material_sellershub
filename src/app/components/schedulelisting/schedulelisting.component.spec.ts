import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchedulelistingComponent } from './schedulelisting.component';

describe('SchedulelistingComponent', () => {
  let component: SchedulelistingComponent;
  let fixture: ComponentFixture<SchedulelistingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchedulelistingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulelistingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
