import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearsIntegrationComponent } from './sears-integration.component';

describe('SearsIntegrationComponent', () => {
  let component: SearsIntegrationComponent;
  let fixture: ComponentFixture<SearsIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearsIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearsIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
