import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsBarcodemanagementComponent } from './settings-barcodemanagement.component';

describe('SettingsBarcodemanagementComponent', () => {
  let component: SettingsBarcodemanagementComponent;
  let fixture: ComponentFixture<SettingsBarcodemanagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsBarcodemanagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsBarcodemanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
