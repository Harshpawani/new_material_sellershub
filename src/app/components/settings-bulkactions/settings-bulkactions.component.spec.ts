import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsBulkactionsComponent } from './settings-bulkactions.component';

describe('SettingsBulkactionsComponent', () => {
  let component: SettingsBulkactionsComponent;
  let fixture: ComponentFixture<SettingsBulkactionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsBulkactionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsBulkactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
