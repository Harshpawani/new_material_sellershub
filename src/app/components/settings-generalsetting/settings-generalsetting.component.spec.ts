import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsGeneralsettingComponent } from './settings-generalsetting.component';

describe('SettingsGeneralsettingComponent', () => {
  let component: SettingsGeneralsettingComponent;
  let fixture: ComponentFixture<SettingsGeneralsettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsGeneralsettingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsGeneralsettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
