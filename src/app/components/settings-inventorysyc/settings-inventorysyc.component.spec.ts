import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsInventorysycComponent } from './settings-inventorysyc.component';

describe('SettingsInventorysycComponent', () => {
  let component: SettingsInventorysycComponent;
  let fixture: ComponentFixture<SettingsInventorysycComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsInventorysycComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsInventorysycComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
