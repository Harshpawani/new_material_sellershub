import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsInvoiceComponent } from './settings-invoice.component';

describe('SettingsInvoiceComponent', () => {
  let component: SettingsInvoiceComponent;
  let fixture: ComponentFixture<SettingsInvoiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsInvoiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
