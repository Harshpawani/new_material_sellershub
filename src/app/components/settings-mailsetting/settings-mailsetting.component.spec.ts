import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsMailsettingComponent } from './settings-mailsetting.component';

describe('SettingsMailsettingComponent', () => {
  let component: SettingsMailsettingComponent;
  let fixture: ComponentFixture<SettingsMailsettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsMailsettingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsMailsettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
