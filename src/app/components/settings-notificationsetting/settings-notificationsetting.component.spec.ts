import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsNotificationsettingComponent } from './settings-notificationsetting.component';

describe('SettingsNotificationsettingComponent', () => {
  let component: SettingsNotificationsettingComponent;
  let fixture: ComponentFixture<SettingsNotificationsettingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsNotificationsettingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsNotificationsettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
