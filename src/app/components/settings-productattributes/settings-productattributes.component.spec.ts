import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsProductattributesComponent } from './settings-productattributes.component';

describe('SettingsProductattributesComponent', () => {
  let component: SettingsProductattributesComponent;
  let fixture: ComponentFixture<SettingsProductattributesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsProductattributesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsProductattributesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
