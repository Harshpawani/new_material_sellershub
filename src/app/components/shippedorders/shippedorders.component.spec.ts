import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippedordersComponent } from './shippedorders.component';

describe('ShippedordersComponent', () => {
  let component: ShippedordersComponent;
  let fixture: ComponentFixture<ShippedordersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShippedordersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippedordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
