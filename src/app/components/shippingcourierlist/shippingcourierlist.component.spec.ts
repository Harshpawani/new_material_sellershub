import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippingcourierlistComponent } from './shippingcourierlist.component';

describe('ShippingcourierlistComponent', () => {
  let component: ShippingcourierlistComponent;
  let fixture: ComponentFixture<ShippingcourierlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShippingcourierlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippingcourierlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
