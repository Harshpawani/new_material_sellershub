import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippinglabelComponent } from './shippinglabel.component';

describe('ShippinglabelComponent', () => {
  let component: ShippinglabelComponent;
  let fixture: ComponentFixture<ShippinglabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShippinglabelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippinglabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
