import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippingrulesComponent } from './shippingrules.component';

describe('ShippingrulesComponent', () => {
  let component: ShippingrulesComponent;
  let fixture: ComponentFixture<ShippingrulesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShippingrulesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippingrulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
