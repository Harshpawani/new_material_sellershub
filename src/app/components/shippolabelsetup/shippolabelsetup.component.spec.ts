import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippolabelsetupComponent } from './shippolabelsetup.component';

describe('ShippolabelsetupComponent', () => {
  let component: ShippolabelsetupComponent;
  let fixture: ComponentFixture<ShippolabelsetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShippolabelsetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippolabelsetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
