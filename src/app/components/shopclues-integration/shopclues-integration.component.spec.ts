import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopcluesIntegrationComponent } from './shopclues-integration.component';

describe('ShopcluesIntegrationComponent', () => {
  let component: ShopcluesIntegrationComponent;
  let fixture: ComponentFixture<ShopcluesIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShopcluesIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopcluesIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
