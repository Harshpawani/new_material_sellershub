import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shopclues-integration',
  templateUrl: './shopclues-integration.component.html',
  styleUrls: ['./shopclues-integration.component.css']
})
export class ShopcluesIntegrationComponent implements OnInit {

  title: string;
  logo: string;
  constructor(private router: Router) { 
    this.logo = this.router.getCurrentNavigation().extras.state.logo;
    this.title = this.router.getCurrentNavigation().extras.state.title;
  }

  ngOnInit(): void {
  }

}
