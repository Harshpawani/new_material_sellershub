import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopeeIntegrationComponent } from './shopee-integration.component';

describe('ShopeeIntegrationComponent', () => {
  let component: ShopeeIntegrationComponent;
  let fixture: ComponentFixture<ShopeeIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShopeeIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopeeIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
