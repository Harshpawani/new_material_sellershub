import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopifyEcommerceComponent } from './shopify-ecommerce.component';

describe('ShopifyEcommerceComponent', () => {
  let component: ShopifyEcommerceComponent;
  let fixture: ComponentFixture<ShopifyEcommerceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShopifyEcommerceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopifyEcommerceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
