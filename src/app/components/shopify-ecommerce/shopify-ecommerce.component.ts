import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shopify-ecommerce',
  templateUrl: './shopify-ecommerce.component.html',
  styleUrls: ['./shopify-ecommerce.component.css']
})
export class ShopifyEcommerceComponent implements OnInit {

  titleEcommrce: string;
  logoEcommerce: string;
  constructor(private router: Router) { 
    this.logoEcommerce = this.router.getCurrentNavigation().extras.state.logo;
    this.titleEcommrce = this.router.getCurrentNavigation().extras.state.title;
    console.log("Title From Eco: " + this.titleEcommrce);
  }

  ngOnInit(): void {
  }

}
