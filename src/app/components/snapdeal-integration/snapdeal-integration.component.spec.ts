import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SnapdealIntegrationComponent } from './snapdeal-integration.component';

describe('SnapdealIntegrationComponent', () => {
  let component: SnapdealIntegrationComponent;
  let fixture: ComponentFixture<SnapdealIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SnapdealIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SnapdealIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
