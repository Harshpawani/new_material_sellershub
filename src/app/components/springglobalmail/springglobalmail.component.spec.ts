import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpringglobalmailComponent } from './springglobalmail.component';

describe('SpringglobalmailComponent', () => {
  let component: SpringglobalmailComponent;
  let fixture: ComponentFixture<SpringglobalmailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpringglobalmailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpringglobalmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
