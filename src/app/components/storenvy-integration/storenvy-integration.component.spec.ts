import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StorenvyIntegrationComponent } from './storenvy-integration.component';

describe('StorenvyIntegrationComponent', () => {
  let component: StorenvyIntegrationComponent;
  let fixture: ComponentFixture<StorenvyIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StorenvyIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StorenvyIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
