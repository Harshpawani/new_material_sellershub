import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StreetIntegrationComponent } from './street-integration.component';

describe('StreetIntegrationComponent', () => {
  let component: StreetIntegrationComponent;
  let fixture: ComponentFixture<StreetIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StreetIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StreetIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
