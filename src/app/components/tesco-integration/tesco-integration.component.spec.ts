import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TescoIntegrationComponent } from './tesco-integration.component';

describe('TescoIntegrationComponent', () => {
  let component: TescoIntegrationComponent;
  let fixture: ComponentFixture<TescoIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TescoIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TescoIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
