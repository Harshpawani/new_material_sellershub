import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tesco-integration',
  templateUrl: './tesco-integration.component.html',
  styleUrls: ['./tesco-integration.component.css']
})
export class TescoIntegrationComponent implements OnInit {

  title: string;
  logo: string;
  constructor(private router: Router) { 
    this.logo = this.router.getCurrentNavigation().extras.state.logo;
    this.title = this.router.getCurrentNavigation().extras.state.title;
  }

  ngOnInit(): void {
  }

}
