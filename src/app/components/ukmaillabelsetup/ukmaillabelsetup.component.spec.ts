import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UkmaillabelsetupComponent } from './ukmaillabelsetup.component';

describe('UkmaillabelsetupComponent', () => {
  let component: UkmaillabelsetupComponent;
  let fixture: ComponentFixture<UkmaillabelsetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UkmaillabelsetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UkmaillabelsetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
