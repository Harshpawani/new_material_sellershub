import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UkmailsettingsComponent } from './ukmailsettings.component';

describe('UkmailsettingsComponent', () => {
  let component: UkmailsettingsComponent;
  let fixture: ComponentFixture<UkmailsettingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UkmailsettingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UkmailsettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
