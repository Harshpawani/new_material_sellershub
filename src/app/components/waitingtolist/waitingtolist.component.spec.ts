import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitingtolistComponent } from './waitingtolist.component';

describe('WaitingtolistComponent', () => {
  let component: WaitingtolistComponent;
  let fixture: ComponentFixture<WaitingtolistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WaitingtolistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitingtolistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
