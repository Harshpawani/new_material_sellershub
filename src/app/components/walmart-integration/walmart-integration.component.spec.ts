import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WalmartIntegrationComponent } from './walmart-integration.component';

describe('WalmartIntegrationComponent', () => {
  let component: WalmartIntegrationComponent;
  let fixture: ComponentFixture<WalmartIntegrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WalmartIntegrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WalmartIntegrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
