import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouseStocksummaryComponent } from './warehouse-stocksummary.component';

describe('WarehouseStocksummaryComponent', () => {
  let component: WarehouseStocksummaryComponent;
  let fixture: ComponentFixture<WarehouseStocksummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WarehouseStocksummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseStocksummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
