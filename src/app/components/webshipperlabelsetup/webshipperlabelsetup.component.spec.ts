import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WebshipperlabelsetupComponent } from './webshipperlabelsetup.component';

describe('WebshipperlabelsetupComponent', () => {
  let component: WebshipperlabelsetupComponent;
  let fixture: ComponentFixture<WebshipperlabelsetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WebshipperlabelsetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebshipperlabelsetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
