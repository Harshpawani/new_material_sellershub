import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WndirectComponent } from './wndirect.component';

describe('WndirectComponent', () => {
  let component: WndirectComponent;
  let fixture: ComponentFixture<WndirectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WndirectComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WndirectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
