import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WooEcommerceComponent } from './woo-ecommerce.component';

describe('WooEcommerceComponent', () => {
  let component: WooEcommerceComponent;
  let fixture: ComponentFixture<WooEcommerceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WooEcommerceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WooEcommerceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
