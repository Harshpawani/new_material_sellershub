import { TestBed } from '@angular/core/testing';

import { UrlAPIService } from './url-api.service';

describe('UrlAPIService', () => {
  let service: UrlAPIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UrlAPIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
