import { Injectable } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { ChannelIntegration } from './channel-integration';

@Injectable({
  providedIn: 'root'
})
export class UrlAPIService {

  BASE_URL: string = "";
  BASE_URL_DUMMY: string = "assets/json-data/";
  
  constructor(private http: HttpClient) {}

  getChannelIntegrations(params){
    return this.http.get<ChannelIntegration[]>(this.BASE_URL_DUMMY + params);
  }

  
}