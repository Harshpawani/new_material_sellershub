import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import PerfectScrollbar from 'perfect-scrollbar';

declare const $: any;

//Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [
    // {
    //     path: '/dashboard',
    //     title: 'Dashboard',
    //     type: 'link',
    //     icontype: 'dashboard'
    // },
    {
        path: '/components',
        title: 'Reports',
        type: 'sub',
        icontype: 'apps',
        collapse: 'components',
        children: [
            {path: 'report-salesbyproducts', title: 'Sales By Report', ab:'B'},
            {path: 'report-lowstock', title: 'Low Stock', ab:'GS'},
            {path: 'report-stockvalue', title: 'Stock Value Report', ab:'P'},
            {path: 'report-orderhistory', title: 'Order History', ab:'SA'}
        ]
    },
    // {
    //     path: '/components',
    //     title: 'Orders',
    //     type: 'sub',
    //     icontype: 'content_paste',
    //     collapse: 'forms',
    //     children: [
    //         {path: 'ordersummary', title: 'Orders Summary', ab:'OS'},
    //         {path: 'sales-orders', title: 'Sales Orders', ab:'SO'},
    //         {path: 'mcforders', title: 'MCF Orders', ab:'MO'},
    //         {path: 'shippedorders', title: 'Shipped Orders', ab:'W'},
    //         {path: 'returnedorders', title: 'Returned Orders', ab:'W'},
    //         {path: 'cancelledorders', title: 'Canceled Orders', ab:'W'},
    //         {path: 'managereturn', title: 'Manage Returns', ab:'W'}
    //     ]
    // },
    {
        path: '/components',
        title: 'Inventory',
        type: 'sub',
        icontype: 'grid_on',
        collapse: 'tables',
        children: [
            {path: 'products', title: 'Products', ab:'RT'},
            {path: 'channelproducts', title: 'Channel Products', ab:'ET'},
            {path: 'stockview', title: 'Stock View', ab:'DT'},
            {path: 'purchaseorder', title: 'Purchase Order', ab:'DT'}
        ]
    },{
        path: '/components',
        title: 'Listings',
        type: 'sub',
        icontype: 'place',
        collapse: 'maps',
        children: [
            {path: 'google', title: 'Channel Listing', ab:'GM'},
            {path: 'schedulelisting', title: 'Channel Scheduled Listings', ab:'FSM'},
            {path: 'vector', title: 'Manage FBA', ab:'VM'}
        ]
    },{
        path: '/components/suppliers',
        title: 'Suppliers',
        type: 'link',
        icontype: 'widgets'

    },
    {
        path: '/components',
        title: 'Warehouse Management',
        type: 'sub',
        icontype: 'image',
        collapse: 'components',
        children: [
            {path: 'warehouses', title: 'Warehouses', ab:'P'},
            {path: 'warehouse-stocksummary', title: 'Stock Summary', ab:'TP'},
            {path: 'warehouse-transfer', title: 'Transfer', ab:'LP'}
        ]
    },
    {
        path: '/components/customers',
        title: 'Customers',
        type: 'link',
        icontype: 'timeline'

    },
    {
        path: '/components',
        title: 'Settings',
        type: 'sub',
        icontype: 'image',
        collapse: 'components',
        children: [
            {path: 'general-setting', title: 'General Settings', ab:'P'},
            {path: 'product-attributes', title: 'Product Atributes', ab:'TP'},
            {path: 'bulk-actions', title: 'Bulk Actions', ab:'LP'},
            {path: 'barcode-management', title: 'Barcode Management', ab:'LP'},
            {path: 'inventory-synchronize', title: 'Inventory Syncronise', ab:'LP'},
            {path: 'invoice-shiping-label', title: 'Invoice / Shipping Labels', ab:'LP'},
            {path: 'mail-setting', title: 'Email Setting', ab:'LP'},
            {path: 'notification-setting', title: 'Notification', ab:'LP'}
        ]
    },
    {
        path: '/components',
        title: 'Integrations',
        type: 'sub',
        icontype: 'image',
        collapse: 'pages',
        children: [
            {path: 'channelconnector', title: 'Channel Integrations', ab:'P'},
            {path: 'shippingcourierlist', title: 'Shipping Courier Setup', ab:'TP'}
        ]
    },
    {
        path: '/components',
        title: 'Template Designer',
        type: 'sub',
        icontype: 'image',
        collapse: 'components',
        children: [
            {path: 'invoicetemplates', title: 'Invoice & Shipping Labels', ab:'P'},
            {path: 'invoice', title: 'Invoice Template(New)', ab:'TP'},
            {path: 'shippinglabel', title: 'Shipping Label(New)', ab:'LP'},
            {path: 'pickuplist', title: 'Pickup List', ab:'LP'},
            {path: 'packlist', title: 'Pack List', ab:'LP'}
        ]
    },
];
@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    menuData;
    ps: any;

    constructor(private router: Router) {}

    /**
   * Modififed by: Aminul Aprijal
   * 23-09-2021
   */

  orderLink(menuTitle, menuLink) {
      console.log('Test Running: ' + menuTitle);
    this.router.navigateByUrl(menuLink);
    var isExistData = JSON.parse(localStorage.getItem("AllTabMenu"));
    if(isExistData == null) isExistData = [];

    let JSONUsers = {
        "menuTitle": menuTitle, 
        "menuLink": menuLink
    }

    localStorage.setItem("TabMenu", JSON.stringify(JSONUsers));
    isExistData.push(JSONUsers);
    localStorage.setItem("AllTabMenu", JSON.stringify(isExistData));
    this.getMenu();
  }
  
  getMenu() {
    this.menuData = JSON.parse(localStorage.getItem("AllTabMenu"));
    console.log(this.menuData);
  }

  removeMenu(menuTitle, menuLink){
    var dataUser = JSON.parse(localStorage.getItem("AllTabMenu"));
        var index = dataUser.findIndex(item => item.userName === menuLink);
        console.log('Index: ' + index);
        dataUser.splice(index, 1);
        localStorage.setItem("AllTabMenu", JSON.stringify(dataUser));
    this.getMenu();
  }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            this.ps = new PerfectScrollbar(elemSidebar);
        }
    }
    updatePS(): void  {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            this.ps.update();
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }
}
